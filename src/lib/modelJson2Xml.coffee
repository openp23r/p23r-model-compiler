#
# @Author goa
#

#
# Todo: Commenting code
#

DomainmodelJson2xml = exports? and exports or @DomainmodelJson2xml = {}

DomainmodelJson2xml.CR = '\n'

DomainmodelJson2xml.definedMetaInformation = {}
DomainmodelJson2xml.localNamespace = ""

DomainmodelJson2xml.xsdTypes = [
	'anyURI', 'boolean', 'date', 'dateTime', 'decimal', 'duration', 'float',
	'gDay', 'gMonth', 'gMonthDay', 'gYear', 'gYearMonth',
	'ID', 'IDREF', 'IDREFS', 'int', 'language', 'Name', 'negativeInteger',
	'nonNegativeInteger', 'nonPositiveInteger', 'normalizedString', 'positiveInteger',
	'QName', 'string', 'time',
	'hexBinary', 'base64Binary', 'text'
]

DomainmodelJson2xml.toXsdType = (type) ->
	t = type.toLowerCase()
	for x in DomainmodelJson2xml.xsdTypes
		if t == x.toLowerCase()
			return x
	return type

DomainmodelJson2xml.xsdSimpleTypes = [
	'anyURI', 'boolean', 'date', 'dateTime', 'decimal', 'duration', 'float',
	'gDay', 'gMonth', 'gMonthDay', 'gYear', 'gYearMonth',
	'id', 'idRef', 'idRefs', 'int', 'language', 'Name', 'negativeInteger',
	'nonNegativeInteger', 'nonPositiveInteger', 'normalizedString', 'positiveInteger',
	'qName', 'string', 'time',
	'hexBinary'
]

DomainmodelJson2xml.xsdComplexTypes = [
	'base64Binary', 'text', 'Base64Binary', 'Text',
	'AnyURI', 'Boolean', 'Date', 'DateTime', 'Decimal', 'Double', 'Duration', 'Float',
	'GDay', 'GMonth', 'GMonthDay', 'GYear', 'GYearMonth',
	'Id', 'IdRef', 'IdRefs', 'Int', 'Language', 'Name', 'NegativeInteger',
	'NonNegativeInteger', 'NonPositiveInteger', 'NormalizedString', 'PositiveInteger',
	'QName', 'String', 'Time',
	'HexBinary'
]

DomainmodelJson2xml.toFirstUpper = (s) ->
	f = s.charAt(0)
	return f.toUpperCase() + s.substring(1)

DomainmodelJson2xml.toFirstLower = (s) ->
	f = s.charAt(0)
	return f.toLowerCase() + s.substring(1)

DomainmodelJson2xml.toOneSet = (s) ->
	out = {}
	x = for y in s
		for i,v of y
			out[i] = v
	return out

DomainmodelJson2xml.comments = (comments) ->
	if (comments?.join("").trim() or "").length > 0
		return "<xs:annotation><xs:documentation>" + DomainmodelJson2xml.CR +
			comments.toString().replace(/</g,"&lt;").replace(/>/g,"&gt;") + DomainmodelJson2xml.CR +
			"</xs:documentation></xs:annotation>" + DomainmodelJson2xml.CR
	else
		return ""

DomainmodelJson2xml.number = (number) ->
	return number

DomainmodelJson2xml.string = (string, language) ->
	return string

DomainmodelJson2xml.pattern = (pattern) ->
	return pattern

DomainmodelJson2xml.identifier = (id) ->
	return id

DomainmodelJson2xml.url = (url) ->
	return url

DomainmodelJson2xml.host = (host) ->
	return host

DomainmodelJson2xml.port = (port) ->
	return port

DomainmodelJson2xml.hashpart = (hashpart) ->
	return hashpart

DomainmodelJson2xml.clause = (comment, content) ->
	content = "" if not content
	return content

DomainmodelJson2xml.main = (finalComments, main) ->
	return main

DomainmodelJson2xml.model = (startComments, model, ns, release, metaInformation, externalTypes, entities, finalComments, annotations) ->
	baseURI = if DomainmodelJson2xml.definedMetaInformation['base'].length then DomainmodelJson2xml.definedMetaInformation['base'][0] else ''
	baseURI = 'http://leitstelle.p23r.de/NS/' if baseURI is ''

	m = model.split('.')
	modelName = m.pop()
	modelPath = if m.length then (m.join('/') + '/') else ''

	qualifiedNamespaces = for x in externalTypes
		'xmlns:' + x.name + '="' + x.url + '" '

	requires = for x in externalTypes
		x.name + ' = require "' + x.name + '"'

	entityCodes = for x in entities
		modelName + '.' + x.name + ' = (source) ->' + DomainmodelJson2xml.CR +
		DomainmodelJson2xml.indent(x.code)

	commonCode = """
		getAttributeValue = (name, source) ->
		  if source? and source.length > 0
		    values = for x in source
		      (
		        for k,v of x
		          if k == ('@' + name)
		            if typeof(v) == 'undefined'
		              ''
		            else if v instanceof Array
		              v.join " "
		            else
		              v
		      )
		    return values.join(" ")
		  else
		    return ''

		hasAttribute = (name, source) ->
		  if source? and source.length > 0
		    for x in source
		      for k,v of x
		        if k == ('@' + name)
		          return true
		    return false
		  else if source?
		    return true
		  else
		    return false

		getElements = (name, source) ->
		  elements = []
		  if source? and source.length > 0
		    values = for x in source
		      (
		        for k,v of x
		          if k == name
		            if v instanceof Array
		              for y in v
		                results.push(y)
		            else
		              results.push(v)
		      )
		  return elements

		getElementValues = (content) ->
		  results = []
		  for x in content
		    for k,v of x
		      if k == '#text'
		        results.push(v)
		      else if k == '#cdata'
		        results.push('<![CDATA[' + v + ']]>')
		  return results

		hasElement = (name, source) ->
		  if source? and source.length > 0
		    for x in source
		      for k,v of x
		        if k == (name)
		          return true
		    return false
		  else if source?
		    return true
		  else
		    return false

	"""

	return requires.join(DomainmodelJson2xml.CR) + DomainmodelJson2xml.CR + DomainmodelJson2xml.CR +
		commonCode + DomainmodelJson2xml.CR +
		entityCodes.join(DomainmodelJson2xml.CR + DomainmodelJson2xml.CR) + DomainmodelJson2xml.CR + DomainmodelJson2xml.CR

DomainmodelJson2xml.indent = (code, inc) ->
	inc = 1 if not inc
	out = for x in [1..inc]
		"  "
	indention = out.join("")
	lines = for x in code.split('\n')
		indention + x
	return lines.join(DomainmodelJson2xml.CR)

DomainmodelJson2xml.metaInformation = (text, values) ->
	DomainmodelJson2xml.definedMetaInformation[text] = values
	return

DomainmodelJson2xml.metaInformationPair = (text1, text2, values) ->
	DomainmodelJson2xml.definedMetaInformation[text1 + ' ' + text2] = values
	return

DomainmodelJson2xml.namespace = (ns) ->
	DomainmodelJson2xml.localNamespace = ns

	return ns

DomainmodelJson2xml.externalType = (name, url, file) ->
	return { name:name, url:url, file:file }

DomainmodelJson2xml.complexEntity = (comments, name, type, prefix, options, isInstance, annotations, entityFeatures) ->
	elements = for x in entityFeatures when (x.types.length is 1 and x.types[0]['type'] not in DomainmodelJson2xml.xsdSimpleTypes) or x.options.isMany is true
		DomainmodelJson2xml.entityFeatureElement(x)
	choices= for x in entityFeatures when x.types.length > 1
		DomainmodelJson2xml.entityFeatureChoices(x)
	attributes = for x in entityFeatures when x.types.length is 1 and x.types[0]['type'] in DomainmodelJson2xml.xsdSimpleTypes and not x.options.isMany is true
		DomainmodelJson2xml.entityFeatureAttribute(x)

	return { name: name, code:
		DomainmodelJson2xml.indent('<xs:complexType name="' + name + '" ' +
			(if type and type isnt '*' then 'mixed="true" ' else '') +
			' >' + DomainmodelJson2xml.CR +
			comments + DomainmodelJson2xml.CR, 1
		) +
		(if elements.length or choices.length or type is '*' then DomainmodelJson2xml.indent('<xs:sequence>', 2) + DomainmodelJson2xml.CR else '') +
		(if elements.length then elements.join('') else '') +
		(if choices.length then choices.join('') else '') +
		(if type is '*' then DomainmodelJson2xml.indent('<xs:any minOccurs="0"/>',3) + DomainmodelJson2xml.CR else '') +
		(if elements.length or choices.length or type is '*' then DomainmodelJson2xml.indent('</xs:sequence>', 2) + DomainmodelJson2xml.CR else '') +
		(if attributes.length then attributes.join(DomainmodelJson2xml.CR) else '') +
		DomainmodelJson2xml.indent('</xs:complexType>' + DomainmodelJson2xml.CR, 1)
	}

DomainmodelJson2xml.element = (comments, name, type, isOptional, isMany, defaultValue, len, isInstance, pattern, range, enums) ->
	return """( (source) ->
		""" + DomainmodelJson2xml.indent(
		(if pattern then """

		  if hasElement('#{name}', source) and not /#{pattern}/.test(getElementValue('#{name}', source)) then
		    throw new Error("element '#{name}' does not match given pattern")

		""" else "" ) +
		(if enums.length > 0 then """

		  if hasElement('#{name}', source) and getElementValue('#{name}', source) in #{JSON.stringify(enums)} then
		    throw new Error("element '#{name}' has no valid value")

		""" else "" ) + """

		  if hasElement('#{name}', source)
		    return '<#{name}>' + getElementValue('#{name}', source) + '</#{name}>'
		""" +
		(if not isOptional and not defaultValue then """

		  else
		    throw new Error("element '#{name}' is missing")
		""" else "" ) +
		(if not isOptional and defaultValue then """

		  else
		    return ' #{name}="#{defaultValue}"'
		""" else "" ) +
		(if isOptional then """

		  else
		    return ''
		""" else "" ) ) + """

		)(source)
		"""
	if len or pattern or enums.length or (range and range.min)
		return DomainmodelJson2xml.indent('<xs:element name="' + name + '"' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' >', 3) + DomainmodelJson2xml.CR + comments + DomainmodelJson2xml.CR +
			DomainmodelJson2xml.indent('<xs:simpleType>', 4) + DomainmodelJson2xml.CR +
			DomainmodelJson2xml.restrictions(type, len, pattern, range, enums, 5) +
			DomainmodelJson2xml.indent('</xs:simpleType>', 4) + DomainmodelJson2xml.CR +
			DomainmodelJson2xml.indent('</xs:element>', 3) + DomainmodelJson2xml.CR
	else
		return DomainmodelJson2xml.indent('<xs:element name="' + name + '"' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' type="' + type + '"' +
			' />' + DomainmodelJson2xml.CR, 3)

DomainmodelJson2xml.entityFeatureElement = (entityFeature) ->
	name = entityFeature.name
	type = entityFeature.types[0]['type']
	prefix = entityFeature.types[0]['prefix']
	isInstance = entityFeature.types[0]['isInstance'] is true
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	len = entityFeature.options.len or 0
	pattern = entityFeature.options.pattern or ''
	range = entityFeature.options.range or {}
	enums = entityFeature.options.enums or []
	comments = entityFeature.comments or ''

	if type is '*'
		return DomainmodelJson2xml.indent('<xs:element name="' + name + '" type="xs:anyType" ' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			'>' + DomainmodelJson2xml.CR +
			comments +
			'</xs:element>' + DomainmodelJson2xml.CR, 3)
	else if prefix and isInstance is true
		return DomainmodelJson2xml.indent('<xs:element name="' + name + '" >' + DomainmodelJson2xml.CR + comments + DomainmodelJson2xml.CR +
			'<xs:complexType>' + DomainmodelJson2xml.CR +
			'<xs:sequence>' + DomainmodelJson2xml.CR +
			'<xs:element' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			' ref="' + prefix + ':' + type + '"' +
			' />' + DomainmodelJson2xml.CR +
			'</xs:sequence>' + DomainmodelJson2xml.CR +
			'</xs:complexType>' + DomainmodelJson2xml.CR +
			'</xs:element>' + DomainmodelJson2xml.CR, 3)
	else if prefix
		return DomainmodelJson2xml.element(comments, name, prefix + ':' + type,
			isOptional, isMany, defaultValue, len, isInstance , pattern, range, enums)
	else if (type in DomainmodelJson2xml.xsdComplexTypes or type in DomainmodelJson2xml.xsdSimpleTypes)
		return DomainmodelJson2xml.element(comments, name, 'xs:' + DomainmodelJson2xml.toXsdType(type),
			isOptional, isMany, defaultValue, len, isInstance, pattern, range, enums)
	else
		return DomainmodelJson2xml.element(comments, name, DomainmodelJson2xml.localNamespace + ':' + type,
			isOptional, isMany, defaultValue, len, isInstance , pattern, range, enums)

DomainmodelJson2xml.entityFeatureChoices = (entityFeature) ->
	choices = for x in entityFeature.types when x
		DomainmodelJson2xml.entityFeatureChoice(x['type'], x['prefix'], x['isInstance'] is true)

	name = entityFeature.name
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	return DomainmodelJson2xml.indent('<xs:element name="' + name + '"' +
		(if isOptional is true then ' minOccurs="0"' else "") +
		(if isMany is true then ' maxOccurs="unbounded"' else "") +
		(if defaultValue then ' default="' + defaultValue + '"' else "") +
		' >', 3) + DomainmodelJson2xml.CR +
		DomainmodelJson2xml.indent('<xs:complexType>',4) + DomainmodelJson2xml.CR +
		DomainmodelJson2xml.indent('<xs:choice>',5) + DomainmodelJson2xml.CR +
		choices.join('') +
		DomainmodelJson2xml.indent('</xs:choice>',5) + DomainmodelJson2xml.CR +
		DomainmodelJson2xml.indent('</xs:complexType>',4) + DomainmodelJson2xml.CR +
		DomainmodelJson2xml.indent('</xs:element>', 3) + DomainmodelJson2xml.CR

DomainmodelJson2xml.entityFeatureChoice = (type, prefix, isInstance) ->
	if type is '*'
		return DomainmodelJson2xml.indent('<xs:element name="any" type="xs:anyType" />' + DomainmodelJson2xml.CR, 6)
	else if prefix and isInstance is true
		return DomainmodelJson2xml.indent('<xs:element name="' + type + '" >' + DomainmodelJson2xml.CR +
			'<xs:complexType>' + DomainmodelJson2xml.CR +
			'<xs:sequence>' + DomainmodelJson2xml.CR +
			'<xs:element' +
			' ref="' + prefix + ':' + type + '"' +
			' />' + DomainmodelJson2xml.CR +
			'</xs:sequence>' + DomainmodelJson2xml.CR +
			'</xs:complexType>' + DomainmodelJson2xml.CR +
			'</xs:element>' + DomainmodelJson2xml.CR, 6)
	else if prefix
		return DomainmodelJson2xml.element('', type, prefix + ':' + type,
			false, false, null, null, isInstance , null, null, [])
	else if type in DomainmodelJson2xml.xsdComplexTypes or type in DomainmodelJson2xml.xsdSimpleTypes
		return DomainmodelJson2xml.element('', type, 'xs:' + DomainmodelJson2xml.toXsdType(type),
			false, false, null, null, isInstance , null, null, [])
	else
		return DomainmodelJson2xml.element('', DomainmodelJson2xml.toFirstLower(type), DomainmodelJson2xml.localNamespace + ':' + type,
			false, false, null, null, isInstance , null, null, [])

# TODO: range
# TODO: len
#
DomainmodelJson2xml.entityFeatureAttribute = (entityFeature) ->
	name = entityFeature.name
	type = entityFeature.types[0]['type']
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	len = entityFeature.options.len or 0
	pattern = entityFeature.options.pattern or ''
	range = entityFeature.options.range or {}
	enums = entityFeature.options.enums or []

	return """( (source) ->
		""" + DomainmodelJson2xml.indent(
		(if pattern then """

		  if hasAttribute(#{name}, source) and not /#{pattern}/.test(getAttributeValue(#{name}, source)) then
		    throw new Error("attribute '#{name}' does not match given pattern")

		""" else "" ) +
		(if enums.length > 0 then """

		  if hasAttribute('#{name}', source) and getAttributeValue('#{name}', source) in #{JSON.stringify(enums)} then
		    throw new Error("attribute '#{name}' has no valid value")

		""" else "" ) + """

		  if hasAttribute('#{name}', source)
		    return ' #{name}="' + getAttributeValue('#{name}', source) + '"'
		""" +
		(if not isOptional and not defaultValue then """

		  else
		    throw new Error("attribute '#{name}' is missing")
		""" else "" ) +
		(if not isOptional and defaultValue then """

		  else
		    return ' #{name}="#{defaultValue}"'
		""" else "" ) +
		(if isOptional then """

		  else
		    return ''
		""" else "" ) ) + """

		)(source)
		"""

DomainmodelJson2xml.simpleEntity = (comments, name, type, prefix, options, annotations) ->
	options.len = 0 if not options.len
	options.pattern = '' if not options.pattern
	options.range = {} if not options.range
	options.enums = [] if not options.enums

	dataType = if prefix then prefix + ':' + type else type or "xs:string"
	dataType = 'xs:' + DomainmodelJson2xml.toXsdType(type) if not prefix and (type in DomainmodelJson2xml.xsdSimpleTypes or type in DomainmodelJson2xml.xsdComplexTypes)
	option = DomainmodelJson2xml.toOneSet(options)

	return {
		name: name,
		code: """( (source) ->
		""" + DomainmodelJson2xml.indent("""
		  return for x in getElements('#{name}', source)
		    '<#{name}>' + getElementValues(x).join('\n') + '</#{name}>'
		""") + """
		)(source)
		"""
	}

DomainmodelJson2xml.restrictions = (type, len, pattern, range, enums, indent) ->
	enums = [] if not enums
	enumerations = for x in enums
		DomainmodelJson2xml.indent('<xs:enumeration value="' + x + '"/>', indent + 1) + DomainmodelJson2xml.CR

	return DomainmodelJson2xml.indent('<xs:restriction base="'+ type + '">', indent) + DomainmodelJson2xml.CR +
		(if pattern then DomainmodelJson2xml.indent('<xs:pattern value="' + pattern + '" />' + DomainmodelJson2xml.CR , indent + 1) else '') +
		(if len then DomainmodelJson2xml.indent('<xs:maxLength value="' + len + '" />' + DomainmodelJson2xml.CR , indent + 1) else '') +
		(if range and range.min then DomainmodelJson2xml.indent('<xs:minInclusive value="' + range.min + '" />' + DomainmodelJson2xml.CR , indent + 1) else '') +
		(if range and range.min then DomainmodelJson2xml.indent('<xs:maxInclusive value="' + range.max + '" />' + DomainmodelJson2xml.CR , indent + 1) else '') +
		(if enumerations and enumerations.length then enumerations.join('') else '') +
		DomainmodelJson2xml.indent('</xs:restriction>', indent) + DomainmodelJson2xml.CR

DomainmodelJson2xml.range = (min, max) ->
	return { range: { min: min, max: max } }

DomainmodelJson2xml.regex = (pattern) ->
	return { pattern: pattern }

DomainmodelJson2xml.length = (len) ->
	return { len: len }

DomainmodelJson2xml.enumeration = (enums) ->
	return { enums: enums }

DomainmodelJson2xml.complexEntityFeatures = (entityFeatures) ->
	return entityFeatures

DomainmodelJson2xml.isMany = () ->
	return { isMany: true }

DomainmodelJson2xml.isOptional = () ->
	return { isOptional: true }

DomainmodelJson2xml.unit = (unit) ->
	return { unit: unit }

DomainmodelJson2xml.defaultValue = (value) ->
	return { defaultValue: value }

DomainmodelJson2xml.entityFeature = ( comments, name, types, entityFeatureOptions, annotations ) ->
	return { name: name, comments: comments, types: types, options: DomainmodelJson2xml.toOneSet(entityFeatureOptions), annotations: annotations }
