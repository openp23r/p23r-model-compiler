#
# @Author goa
#

#
# Todo: Commenting code
#

Domainmodel2utXsd = exports? and exports or @Domainmodel2utXsd = {}

Domainmodel2utXsd.CR = '\n'

Domainmodel2utXsd.definedMetaInformation = {}
Domainmodel2utXsd.localNamespace = ""

Domainmodel2utXsd.xsdTypes = [
	'anyURI', 'boolean', 'date', 'dateTime', 'decimal', 'duration', 'float',
	'gDay', 'gMonth', 'gMonthDay', 'gYear', 'gYearMonth',
	'ID', 'IDREF', 'IDREFS', 'int', 'language', 'Name', 'negativeInteger',
	'nonNegativeInteger', 'nonPositiveInteger', 'normalizedString', 'positiveInteger',
	'QName', 'string', 'time',
	'hexBinary', 'base64Binary', 'text'
]

Domainmodel2utXsd.toXsdType = (type) ->
	t = type.toLowerCase()
	for x in Domainmodel2utXsd.xsdTypes
		if t == x.toLowerCase()
			return x
	return type

Domainmodel2utXsd.xsdSimpleTypes = [
	'anyURI', 'boolean', 'date', 'dateTime', 'decimal', 'duration', 'float',
	'gDay', 'gMonth', 'gMonthDay', 'gYear', 'gYearMonth',
	'id', 'idRef', 'idRefs', 'int', 'language', 'Name', 'negativeInteger',
	'nonNegativeInteger', 'nonPositiveInteger', 'normalizedString', 'positiveInteger',
	'qName', 'string', 'time',
	'hexBinary'
]

Domainmodel2utXsd.xsdComplexTypes = [
	'base64Binary', 'text', 'Base64Binary', 'Text',
	'AnyURI', 'Boolean', 'Date', 'DateTime', 'Decimal', 'Double', 'Duration', 'Float',
	'GDay', 'GMonth', 'GMonthDay', 'GYear', 'GYearMonth',
	'Id', 'IdRef', 'IdRefs', 'Int', 'Language', 'Name', 'NegativeInteger',
	'NonNegativeInteger', 'NonPositiveInteger', 'NormalizedString', 'PositiveInteger',
	'QName', 'String', 'Time',
	'HexBinary'
]

Domainmodel2utXsd.toFirstUpper = (s) ->
	f = s.charAt(0)
	return f.toUpperCase() + s.substring(1)

Domainmodel2utXsd.toFirstLower = (s) ->
	f = s.charAt(0)
	return f.toLowerCase() + s.substring(1)

Domainmodel2utXsd.toOneSet = (s) ->
	out = {}
	x = for y in s
		for i,v of y
			out[i] = v
	return out

Domainmodel2utXsd.comments = (comments) ->
	if (comments?.join("").trim() or "").length > 0
		return "<xs:annotation><xs:documentation>" + Domainmodel2utXsd.CR +
			comments.toString().replace(/</g,"&lt;").replace(/>/g,"&gt;") + Domainmodel2utXsd.CR +
			"</xs:documentation></xs:annotation>" + Domainmodel2utXsd.CR
	else
		return ""

Domainmodel2utXsd.number = (number) ->
	return number

Domainmodel2utXsd.string = (string, language) ->
	return string

Domainmodel2utXsd.pattern = (pattern) ->
	return pattern

Domainmodel2utXsd.identifier = (id) ->
	return id

Domainmodel2utXsd.url = (url) ->
	return url

Domainmodel2utXsd.host = (host) ->
	return host

Domainmodel2utXsd.port = (port) ->
	return port

Domainmodel2utXsd.hashpart = (hashpart) ->
	return hashpart

Domainmodel2utXsd.frame = (content, finalComments) ->
	return '<?xml version="1.0" encoding="utf-8" ?>' + Domainmodel2utXsd.CR +
		content + Domainmodel2utXsd.CR +
		finalComments +
		'</xs:schema>'

Domainmodel2utXsd.clause = (comment, content) ->
	content = "" if not content
	return content

Domainmodel2utXsd.main = (finalComments, main) ->
	finalComments = "" if not finalComments
	main = "" if not main
	return Domainmodel2utXsd.frame(main, finalComments)

Domainmodel2utXsd.model = (startComments, model, ns, release, metaInformation, externalTypes, entities, finalComments, annotations) ->
	baseURI = if Domainmodel2utXsd.definedMetaInformation['base'].length then Domainmodel2utXsd.definedMetaInformation['base'][0] else ''
	baseURI = 'http://leitstelle.p23r.de/NS/' if baseURI is ''

	m = model.split('.')
	modelName = m.pop()
	modelPath = if m.length then (m.join('/') + '/') else ''

	qualifiedNamespaces = for x in externalTypes
		'xmlns:' + x.name + '="' + x.url + '" '

	imports = for x in externalTypes
		Domainmodel2utXsd.indent('<xs:import namespace="' + x.url + '" schemaLocation="' +
		(if x.file then x.file else x.url) + '" />', 1)

	entityCodes = for x in entities
		x.code

	return '<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" ' +
		'targetNamespace="' + baseURI + modelPath + Domainmodel2utXsd.toFirstUpper(modelName) + release.replace(/\./g, '-') + '" ' +
		'xmlns:' + Domainmodel2utXsd.localNamespace + '="' + baseURI + modelPath + Domainmodel2utXsd.toFirstUpper(modelName) + release.replace(/\./g, '-') + '" ' +
		qualifiedNamespaces.join('') +
		'elementFormDefault="qualified" '+
		'version="' + release + '" ' +
		'>' + Domainmodel2utXsd.CR +
		startComments +
		(if imports.length then Domainmodel2utXsd.CR + imports.join(Domainmodel2utXsd.CR) + Domainmodel2utXsd.CR else '') +
		(if entities.length and entities[0].name is Domainmodel2utXsd.toFirstUpper(modelName) then Domainmodel2utXsd.CR + Domainmodel2utXsd.indent('<xs:element name="' + Domainmodel2utXsd.toFirstLower(modelName) + '" type="' + ns + ':' + Domainmodel2utXsd.toFirstUpper(modelName) + '" />', 1) + Domainmodel2utXsd.CR else '') +
		Domainmodel2utXsd.CR + entityCodes.join(Domainmodel2utXsd.CR)

Domainmodel2utXsd.indent = (xml, inc) ->
	inc = 0 if not inc
	out = for x in [1..inc]
		"  "
	return out.join("") + xml

Domainmodel2utXsd.metaInformation = (text, values) ->
	Domainmodel2utXsd.definedMetaInformation[text] = values
	return

Domainmodel2utXsd.metaInformationPair = (text1, text2, values) ->
	Domainmodel2utXsd.definedMetaInformation[text1 + ' ' + text2] = values
	return

Domainmodel2utXsd.namespace = (ns) ->
	Domainmodel2utXsd.localNamespace = ns

	return ns

Domainmodel2utXsd.externalType = (name, url, file) ->
	return { name:name, url:url, file:file }

Domainmodel2utXsd.complexEntity = (comments, name, type, prefix, options, isInstance, annotations, entityFeatures) ->
	elements = for x in entityFeatures when (x.types.length is 1 and x.types[0]['type'] not in Domainmodel2utXsd.xsdSimpleTypes) or x.options.isMany is true
		Domainmodel2utXsd.entityFeatureElement(x)
	choices= for x in entityFeatures when x.types.length > 1
		Domainmodel2utXsd.entityFeatureChoices(x)
	attributes = for x in entityFeatures when x.types.length is 1 and x.types[0]['type'] in Domainmodel2utXsd.xsdSimpleTypes and not x.options.isMany is true
		Domainmodel2utXsd.entityFeatureAttribute(x)

	return { name: name, code:
		Domainmodel2utXsd.indent('<xs:complexType name="' + name + '" ' +
			(if type and type isnt '*' then 'mixed="true" ' else '') +
			' >' + Domainmodel2utXsd.CR +
			comments + Domainmodel2utXsd.CR, 1
		) +
		(if elements.length or choices.length or type is '*' then Domainmodel2utXsd.indent('<xs:sequence>', 2) + Domainmodel2utXsd.CR else '') +
		(if elements.length then elements.join('') else '') +
		(if choices.length then choices.join('') else '') +
		(if type is '*' then Domainmodel2utXsd.indent('<xs:any minOccurs="0"/>',3) + Domainmodel2utXsd.CR else '') +
		(if elements.length or choices.length or type is '*' then Domainmodel2utXsd.indent('</xs:sequence>', 2) + Domainmodel2utXsd.CR else '') +
		(if attributes.length then attributes.join('') else '') +
		Domainmodel2utXsd.indent('</xs:complexType>' + Domainmodel2utXsd.CR, 1)
	}

Domainmodel2utXsd.element = (comments, name, type, isOptional, isMany, defaultValue, len, isInstance, pattern, range, enums) ->
	if len or pattern or enums.length or (range and range.min)
		return Domainmodel2utXsd.indent('<xs:element name="' + name + '"' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' type="xs:string" >', 3) + Domainmodel2utXsd.CR + comments + Domainmodel2utXsd.CR +
			Domainmodel2utXsd.indent('</xs:element>', 3) + Domainmodel2utXsd.CR
	else
		return Domainmodel2utXsd.indent('<xs:element name="' + name + '"' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' type="' + type + '"' +
			' />' + Domainmodel2utXsd.CR, 3)

Domainmodel2utXsd.entityFeatureElement = (entityFeature) ->
	name = entityFeature.name
	type = entityFeature.types[0]['type']
	prefix = entityFeature.types[0]['prefix']
	isInstance = entityFeature.types[0]['isInstance'] is true
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	len = entityFeature.options.len or 0
	pattern = entityFeature.options.pattern or ''
	range = entityFeature.options.range or {}
	enums = entityFeature.options.enums or []
	comments = entityFeature.comments or ''

	if type is '*'
		return Domainmodel2utXsd.indent('<xs:element name="' + name + '" type="xs:anyType" ' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			'>' + Domainmodel2utXsd.CR +
			comments +
			'</xs:element>' + Domainmodel2utXsd.CR, 3)
	else if prefix and isInstance is true
		return Domainmodel2utXsd.indent('<xs:element name="' + name + '" >' + Domainmodel2utXsd.CR + comments + Domainmodel2utXsd.CR +
			'<xs:complexType>' + Domainmodel2utXsd.CR +
			'<xs:sequence>' + Domainmodel2utXsd.CR +
			'<xs:element' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			' ref="' + prefix + ':' + type + '"' +
			' />' + Domainmodel2utXsd.CR +
			'</xs:sequence>' + Domainmodel2utXsd.CR +
			'</xs:complexType>' + Domainmodel2utXsd.CR +
			'</xs:element>' + Domainmodel2utXsd.CR, 3)
	else if prefix
		return Domainmodel2utXsd.element(comments, name, prefix + ':' + type,
			isOptional, isMany, defaultValue, len, isInstance , pattern, range, enums)
	else if (type in Domainmodel2utXsd.xsdComplexTypes or type in Domainmodel2utXsd.xsdSimpleTypes)
		return Domainmodel2utXsd.element(comments, name, 'xs:string',
			isOptional, isMany, defaultValue, len, isInstance, pattern, range, enums)
	else
		return Domainmodel2utXsd.element(comments, name, Domainmodel2utXsd.localNamespace + ':' + type,
			isOptional, isMany, defaultValue, len, isInstance , pattern, range, enums)

Domainmodel2utXsd.entityFeatureChoices = (entityFeature) ->
	choices = for x in entityFeature.types when x
		Domainmodel2utXsd.entityFeatureChoice(x['type'], x['prefix'], x['isInstance'] is true)

	name = entityFeature.name
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	return Domainmodel2utXsd.indent('<xs:element name="' + name + '"' +
		(if isOptional is true then ' minOccurs="0"' else "") +
		(if isMany is true then ' maxOccurs="unbounded"' else "") +
		(if defaultValue then ' default="' + defaultValue + '"' else "") +
		' >', 3) + Domainmodel2utXsd.CR +
		Domainmodel2utXsd.indent('<xs:complexType>',4) + Domainmodel2utXsd.CR +
		Domainmodel2utXsd.indent('<xs:choice>',5) + Domainmodel2utXsd.CR +
		choices.join('') +
		Domainmodel2utXsd.indent('</xs:choice>',5) + Domainmodel2utXsd.CR +
		Domainmodel2utXsd.indent('</xs:complexType>',4) + Domainmodel2utXsd.CR +
		Domainmodel2utXsd.indent('</xs:element>', 3) + Domainmodel2utXsd.CR

Domainmodel2utXsd.entityFeatureChoice = (type, prefix, isInstance) ->
	if type is '*'
		return Domainmodel2utXsd.indent('<xs:element name="any" type="xs:anyType" />' + Domainmodel2utXsd.CR, 6)
	else if prefix and isInstance is true
		return Domainmodel2utXsd.indent('<xs:element name="' + type + '" >' + Domainmodel2utXsd.CR +
			'<xs:complexType>' + Domainmodel2utXsd.CR +
			'<xs:sequence>' + Domainmodel2utXsd.CR +
			'<xs:element' +
			' ref="' + prefix + ':' + type + '"' +
			' />' + Domainmodel2utXsd.CR +
			'</xs:sequence>' + Domainmodel2utXsd.CR +
			'</xs:complexType>' + Domainmodel2utXsd.CR +
			'</xs:element>' + Domainmodel2utXsd.CR, 6)
	else if prefix
		return Domainmodel2utXsd.element('', type, prefix + ':' + type,
			false, false, null, null, isInstance , null, null, [])
	else if type in Domainmodel2utXsd.xsdComplexTypes or type in Domainmodel2utXsd.xsdSimpleTypes
		return Domainmodel2utXsd.element('', type, 'xs:' + Domainmodel2utXsd.toXsdType(type),
			false, false, null, null, isInstance , null, null, [])
	else
		return Domainmodel2utXsd.element('', Domainmodel2utXsd.toFirstLower(type), Domainmodel2utXsd.localNamespace + ':' + type,
			false, false, null, null, isInstance , null, null, [])

Domainmodel2utXsd.entityFeatureAttribute = (entityFeature) ->
	name = entityFeature.name
	defaultValue = entityFeature.options.defaultValue or ''
	comments = entityFeature.comments or ''

	return Domainmodel2utXsd.indent('<xs:attribute name="' + name + '"' +
		(if defaultValue then ' default="' + defaultValue + '"' else "") +
		' type="xs:string"' +
		' >' + Domainmodel2utXsd.CR + comments + Domainmodel2utXsd.CR + '</xs:attribute>', 2)

Domainmodel2utXsd.range = (min, max) ->
	return { range: { min: min, max: max } }

Domainmodel2utXsd.regex = (pattern) ->
	return { pattern: pattern }

Domainmodel2utXsd.length = (len) ->
	return { len: len }

Domainmodel2utXsd.enumeration = (enums) ->
	return { enums: enums }

Domainmodel2utXsd.complexEntityFeatures = (entityFeatures) ->
	return entityFeatures

Domainmodel2utXsd.isMany = () ->
	return { isMany: true }

Domainmodel2utXsd.isOptional = () ->
	return { isOptional: true }

Domainmodel2utXsd.unit = (unit) ->
	return { unit: unit }

Domainmodel2utXsd.defaultValue = (value) ->
	return { defaultValue: value }

Domainmodel2utXsd.entityFeature = ( comments, name, types, entityFeatureOptions, annotations ) ->
	return { name: name, comments: comments, types: types, options: Domainmodel2utXsd.toOneSet(entityFeatureOptions), annotations: annotations }
