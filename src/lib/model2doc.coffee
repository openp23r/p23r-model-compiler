#
# @Author goa
#
Domainmodel2doc = exports? and exports or @Domainmodel2doc = {}

try
	marked = require("marked")

Domainmodel2doc.comments = (comments) ->
	outs = for comment in comments
		'<div class="comment">' + marked(comment) + '</div>'
	return outs.join("")

Domainmodel2doc.number = (number) ->
	return '<span class="number">' + number + '</span>'

Domainmodel2doc.string = (string, language) ->
	languageClass = if language then ' ' + language + '-lang' else ""
	return '"<span class="string' + languageClass + '">' + string + '</span>"'

Domainmodel2doc.pattern = (pattern) ->
	return '"<span class="regex">' + pattern + '</span>"'

Domainmodel2doc.identifier = (id) ->
	return '<span class="identifier">' + id + '</span>'

Domainmodel2doc.url = (id) ->
	return '<span class="url">' + id + '</span>'

Domainmodel2doc.host = (id) ->
	return '<span class="host">' + id + '</span>'

Domainmodel2doc.port = (id) ->
	return '<span class="port">' + id + '</span>'

Domainmodel2doc.hashpart = (id) ->
	return '<span class="hash">' + id + '</span>'

Domainmodel2doc.frame = (content) ->
	return '<table class="literate"><tr><th/><th/></tr>' +
		content +
		'</table>'

Domainmodel2doc.clause = (comment, content) ->
	if not comment and not content
		return ''
	comment = "" if not comment
	content = "" if not content
	result = ''
	comments = comment.split(/<div class="comment"><h/)

	for x, i in comments
		if (i == 0) && (i != (comments.length - 1))
			result += '<tr class="section"><td class="headline" colspan="2">' + x + '</div></tr>'
		else if i != (comments.length - 1)
			result += '<tr class="section"><td class="headline" colspan="2"><div class="comment"><h' + x + '</div></tr>'
	if comments.length == 1
		result += '<tr class="section">' +
			'<td class="documentation">' + comments[0] + '</td>' +
			'<td class="content' + '">' + content + '</td>' +
		'</tr>'
	else
		result += '<tr class="section">' +
			'<td class="documentation"><div class="comment"><h' + comments[(comments.length - 1)] + '</td>' +
			'<td class="content' + '">' + content + '</td>' +
		'</tr>'
	return result

Domainmodel2doc.main = (finalComments, main) ->
	finalComments = "" if not finalComments
	main = "" if not main
	return Domainmodel2doc.frame(
		main +
		'<div class="documentation"><p>' + finalComments + '</p></div>'
	)

Domainmodel2doc.model = (startComments, model, ns, release, metaInformation, externalTypes, entities, finalComments, annotations) ->
	return Domainmodel2doc.clause(startComments,
		Domainmodel2doc.modelStart(model, ns, release) +
		Domainmodel2doc.indent(metaInformation,1)) +
	 	externalTypes.join("") +
		entities.join("") +
		Domainmodel2doc.clause(finalComments, Domainmodel2doc.modelEnd())

Domainmodel2doc.modelStart = (model, ns, release) ->
	return '<p>' +
			'<span class="keyword">model</span>' +
			' <span class="identifier modelname">' + model + '</span>' +
			' <span class="keyword">as</span>' +
			' <span class="identifier modelnamespace">' + ns + '</span>' +
			' <span class="keyword">release</span> ' +
			release +
			Domainmodel2doc.openBracket() +
		'</p>'

Domainmodel2doc.indent = (xml, inc) ->
	inc = 0 if not inc
	return '<div class="indent' + inc + '">' + xml + '</div>'

Domainmodel2doc.modelEnd = () ->
	return Domainmodel2doc.closeBracket()

Domainmodel2doc.metaInformation = (text, values) ->
	outs = for v in values when v
		'<p class="metainformation">' + text + ' ' + v + '</p>'
	return outs.join("")

Domainmodel2doc.metaInformationPair = (text1, text2, values) ->
	outs = for v in values
		'<p class="metainformation">' + text1 + ' ' + v[0] + ' ' + text2 + ' ' + v[1] + '</p>'
	return outs.join("")

Domainmodel2doc.openBracket = () ->
	return ' <span class="keyword">{</span>'

Domainmodel2doc.closeBracket = () ->
	return '<span class="keyword">}</span>'

Domainmodel2doc.namespace = (ns) ->
	return ns

Domainmodel2doc.externalType = (name, url, file) ->
	outFile = if file then ' <span class="keyword">file</span> ' + file else ""
	return Domainmodel2doc.indent(
		'<div class="namespace">' +
		'<span class="keyword">namespace</span> ' + name +
		' <span class="keyword">=</span> ' + url +
		outFile +
		'</div>',1)

Domainmodel2doc.simpleEntity = (comments, name, type, prefix, options, isInstance, annotations) ->
	instance = if isInstance then "instance of " else ""
	outOptions = if options then options.join("") else ""

	outType = "" if not type;
	outType = ' : ' + '<span class="type">' + type + '</span> ' if type and not prefix
	outType = ' : ' + instance + '<span class="type">' + prefix + ':' + type + '</span> ' if type and prefix

	return Domainmodel2doc.clause(comments, Domainmodel2doc.indent(
		'<div class="entity">' +
		'<span class="keyword">entity</span> ' +
		name +
		outType +
		outOptions +
		'</div>',1))

Domainmodel2doc.complexEntity = (comments, name, type, prefix, options, isInstance, annotations, entityFeatures) ->
	instance = if isInstance then "instance of " else ""
	outOptions = if options then options.join("") else ""

	outType = "" if not type;
	outType = ' : ' + '<span class="type">' + type + '</span> ' if type and not prefix
	outType = ' : ' + instance + '<span class="type">' + prefix + ':' + type + '</span> ' if type and prefix

	return Domainmodel2doc.clause(comments, Domainmodel2doc.indent(
		'<div class="entity">' +
		'<span class="keyword">entity</span> ' +
		name +
		outType +
		outOptions +
		Domainmodel2doc.openBracket() +
		'</div>',1)) +
		entityFeatures +
		Domainmodel2doc.clause("", Domainmodel2doc.indent(Domainmodel2doc.closeBracket(),1))

Domainmodel2doc.range = (min, max) ->
	return '<span class="keyword">range</span> ' +
		min +
		' <span class="keyword">to</span> ' +
		max +
		' '

Domainmodel2doc.regex = (pattern) ->
	return '<span class="keyword">match</span> ' + pattern + ' '

Domainmodel2doc.length = (len) ->
	return '<span class="keyword">length</span> ' + len + ' '

Domainmodel2doc.enumeration = (enums) ->
	return '<span class="keyword">from</span> ' + enums.join(" ")

Domainmodel2doc.complexEntityFeatures = (entityFeatures) ->
	return entityFeatures.join("")

Domainmodel2doc.instance = () ->
	return '<span class="keyword">instance</span> '

Domainmodel2doc.isMany = () ->
	return '<span class="keyword">many</span> '

Domainmodel2doc.isOptional = () ->
	return '<span class="keyword">optional</span> '

Domainmodel2doc.unit = (unit) ->
	return '<span class="keyword">unit</span> ' + '<span class="identifier">' + unit + '</span> '

Domainmodel2doc.defaultValue = (value) ->
	return '<span class="keyword">default</span> ' + value

Domainmodel2doc.entityFeature = ( comments, name, types, options, annotations ) ->
	if not types
		outType = ""
	else
		outTypes = for [type, prefix, isInstance] in types
			instance = if isInstance then Domainmodel2doc.instance else ""
			if type and not prefix
				instance + '<span class="type">' + type + '</span> '
			else if type and prefix
				instance + '<span class="type">' + prefix + ':' + type + '</span> '
		outType = ' : ' + outTypes.join ' | '

	return Domainmodel2doc.clause(comments, Domainmodel2doc.indent(
		'<div class="entityFeature">' +
		name +
		outType +
		options.join("") +
		'</div>',2)
	)
