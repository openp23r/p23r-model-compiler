#
# @Author goa
#

#
# Todo: Commenting code
#

Domainmodel2xsd = exports? and exports or @Domainmodel2xsd = {}

Domainmodel2xsd.CR = '\n'

Domainmodel2xsd.definedMetaInformation = {}
Domainmodel2xsd.localNamespace = ""

Domainmodel2xsd.xsdTypes = [
	'anyURI', 'boolean', 'date', 'dateTime', 'decimal', 'duration', 'float',
	'gDay', 'gMonth', 'gMonthDay', 'gYear', 'gYearMonth',
	'ID', 'IDREF', 'IDREFS', 'int', 'language', 'Name', 'negativeInteger',
	'nonNegativeInteger', 'nonPositiveInteger', 'normalizedString', 'positiveInteger',
	'QName', 'string', 'time',
	'hexBinary', 'base64Binary', 'text'
]

Domainmodel2xsd.toXsdType = (type) ->
	t = type.toLowerCase()
	for x in Domainmodel2xsd.xsdTypes
		if t == x.toLowerCase()
			return x
	return type

Domainmodel2xsd.xsdSimpleTypes = [
	'anyURI', 'boolean', 'date', 'dateTime', 'decimal', 'duration', 'float',
	'gDay', 'gMonth', 'gMonthDay', 'gYear', 'gYearMonth',
	'id', 'idRef', 'idRefs', 'int', 'language', 'Name', 'negativeInteger',
	'nonNegativeInteger', 'nonPositiveInteger', 'normalizedString', 'positiveInteger',
	'qName', 'string', 'time',
	'hexBinary'
]

Domainmodel2xsd.xsdComplexTypes = [
	'base64Binary', 'text', 'Base64Binary', 'Text',
	'AnyURI', 'Boolean', 'Date', 'DateTime', 'Decimal', 'Double', 'Duration', 'Float',
	'GDay', 'GMonth', 'GMonthDay', 'GYear', 'GYearMonth',
	'Id', 'IdRef', 'IdRefs', 'Int', 'Language', 'Name', 'NegativeInteger',
	'NonNegativeInteger', 'NonPositiveInteger', 'NormalizedString', 'PositiveInteger',
	'QName', 'String', 'Time',
	'HexBinary'
]

Domainmodel2xsd.toFirstUpper = (s) ->
	f = s.charAt(0)
	return f.toUpperCase() + s.substring(1)

Domainmodel2xsd.toFirstLower = (s) ->
	f = s.charAt(0)
	return f.toLowerCase() + s.substring(1)

Domainmodel2xsd.toOneSet = (s) ->
	out = {}
	x = for y in s
		for i,v of y
			out[i] = v
	return out

Domainmodel2xsd.comments = (comments) ->
	if (comments?.join("").trim() or "").length > 0
		return "<xs:annotation><xs:documentation>" + Domainmodel2xsd.CR +
			comments.toString().replace(/</g,"&lt;").replace(/>/g,"&gt;") + Domainmodel2xsd.CR +
			"</xs:documentation></xs:annotation>" + Domainmodel2xsd.CR
	else
		return ""

Domainmodel2xsd.number = (number) ->
	return number

Domainmodel2xsd.string = (string, language) ->
	return string

Domainmodel2xsd.pattern = (pattern) ->
	return pattern

Domainmodel2xsd.identifier = (id) ->
	return id

Domainmodel2xsd.url = (url) ->
	return url

Domainmodel2xsd.host = (host) ->
	return host

Domainmodel2xsd.port = (port) ->
	return port

Domainmodel2xsd.hashpart = (hashpart) ->
	return hashpart

Domainmodel2xsd.frame = (content, finalComments) ->
	return '<?xml version="1.0" encoding="utf-8" ?>' + Domainmodel2xsd.CR +
		content + Domainmodel2xsd.CR +
		finalComments +
		'</xs:schema>'

Domainmodel2xsd.clause = (comment, content) ->
	content = "" if not content
	return content

Domainmodel2xsd.main = (finalComments, main) ->
	finalComments = "" if not finalComments
	main = "" if not main
	return Domainmodel2xsd.frame(main, finalComments)

Domainmodel2xsd.model = (startComments, model, ns, release, metaInformation, externalTypes, entities, finalComments, annotations) ->
	baseURI = if Domainmodel2xsd.definedMetaInformation['base'].length then Domainmodel2xsd.definedMetaInformation['base'][0] else ''
	baseURI = 'http://leitstelle.p23r.de/NS/' if baseURI is ''

	m = model.split('.')
	modelName = m.pop()
	modelPath = if m.length then (m.join('/') + '/') else ''

	qualifiedNamespaces = for x in externalTypes
		'xmlns:' + x.name + '="' + x.url + '" '

	imports = for x in externalTypes
		Domainmodel2xsd.indent('<xs:import namespace="' + x.url + '" schemaLocation="' +
		(if x.file then x.file else x.url) + '" />', 1)

	entityCodes = for x in entities
		x.code

	return '<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" ' +
		'targetNamespace="' + baseURI + modelPath + Domainmodel2xsd.toFirstUpper(modelName) + release.replace(/\./g, '-') + '" ' +
		'xmlns:' + Domainmodel2xsd.localNamespace + '="' + baseURI + modelPath + Domainmodel2xsd.toFirstUpper(modelName) + release.replace(/\./g, '-') + '" ' +
		qualifiedNamespaces.join('') +
		'elementFormDefault="qualified" '+
		'version="' + release + '" ' +
		'>' + Domainmodel2xsd.CR +
		startComments +
		(if imports.length then Domainmodel2xsd.CR + imports.join(Domainmodel2xsd.CR) + Domainmodel2xsd.CR else '') +
		(if entities.length and entities[0].name is Domainmodel2xsd.toFirstUpper(modelName) then Domainmodel2xsd.CR + Domainmodel2xsd.indent('<xs:element name="' + Domainmodel2xsd.toFirstLower(modelName) + '" type="' + ns + ':' + Domainmodel2xsd.toFirstUpper(modelName) + '" />', 1) + Domainmodel2xsd.CR else '') +
		Domainmodel2xsd.CR + entityCodes.join(Domainmodel2xsd.CR)

Domainmodel2xsd.indent = (xml, inc) ->
	inc = 0 if not inc
	out = for x in [1..inc]
		"  "
	return out.join("") + xml

Domainmodel2xsd.metaInformation = (text, values) ->
	Domainmodel2xsd.definedMetaInformation[text] = values
	return

Domainmodel2xsd.metaInformationPair = (text1, text2, values) ->
	Domainmodel2xsd.definedMetaInformation[text1 + ' ' + text2] = values
	return

Domainmodel2xsd.namespace = (ns) ->
	Domainmodel2xsd.localNamespace = ns

	return ns

Domainmodel2xsd.externalType = (name, url, file) ->
	return { name:name, url:url, file:file }

Domainmodel2xsd.complexEntity = (comments, name, type, prefix, options, isInstance, annotations, entityFeatures) ->
	elements = for x in entityFeatures when (x.types.length is 1 and x.types[0]['type'] not in Domainmodel2xsd.xsdSimpleTypes) or x.options.isMany is true
		Domainmodel2xsd.entityFeatureElement(x)
	choices= for x in entityFeatures when x.types.length > 1
		Domainmodel2xsd.entityFeatureChoices(x)
	attributes = for x in entityFeatures when x.types.length is 1 and x.types[0]['type'] in Domainmodel2xsd.xsdSimpleTypes and not x.options.isMany is true
		Domainmodel2xsd.entityFeatureAttribute(x)

	return { name: name, code:
		Domainmodel2xsd.indent('<xs:complexType name="' + name + '" ' +
			(if type and type isnt '*' then 'mixed="true" ' else '') +
			' >' + Domainmodel2xsd.CR +
			comments + Domainmodel2xsd.CR, 1
		) +
		(if elements.length or choices.length or type is '*' then Domainmodel2xsd.indent('<xs:sequence>', 2) + Domainmodel2xsd.CR else '') +
		(if elements.length then elements.join('') else '') +
		(if choices.length then choices.join('') else '') +
		(if type is '*' then Domainmodel2xsd.indent('<xs:any minOccurs="0"/>',3) + Domainmodel2xsd.CR else '') +
		(if elements.length or choices.length or type is '*' then Domainmodel2xsd.indent('</xs:sequence>', 2) + Domainmodel2xsd.CR else '') +
		(if attributes.length then attributes.join('') else '') +
		Domainmodel2xsd.indent('</xs:complexType>' + Domainmodel2xsd.CR, 1)
	}

Domainmodel2xsd.element = (comments, name, type, isOptional, isMany, defaultValue, len, isInstance, pattern, range, enums) ->
	if len or pattern or enums.length or (range and range.min)
		return Domainmodel2xsd.indent('<xs:element name="' + name + '"' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' >', 3) + Domainmodel2xsd.CR + comments + Domainmodel2xsd.CR +
			Domainmodel2xsd.indent('<xs:simpleType>', 4) + Domainmodel2xsd.CR +
			Domainmodel2xsd.restrictions(type, len, pattern, range, enums, 5) +
			Domainmodel2xsd.indent('</xs:simpleType>', 4) + Domainmodel2xsd.CR +
			Domainmodel2xsd.indent('</xs:element>', 3) + Domainmodel2xsd.CR
	else
		return Domainmodel2xsd.indent('<xs:element name="' + name + '"' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' type="' + type + '"' +
			' />' + Domainmodel2xsd.CR, 3)

Domainmodel2xsd.entityFeatureElement = (entityFeature) ->
	name = entityFeature.name
	type = entityFeature.types[0]['type']
	prefix = entityFeature.types[0]['prefix']
	isInstance = entityFeature.types[0]['isInstance'] is true
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	len = entityFeature.options.len or 0
	pattern = entityFeature.options.pattern or ''
	range = entityFeature.options.range or {}
	enums = entityFeature.options.enums or []
	comments = entityFeature.comments or ''

	if type is '*'
		return Domainmodel2xsd.indent('<xs:element name="' + name + '" type="xs:anyType" ' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			'>' + Domainmodel2xsd.CR +
			comments +
			'</xs:element>' + Domainmodel2xsd.CR, 3)
	else if prefix and isInstance is true
		return Domainmodel2xsd.indent('<xs:element name="' + name + '" >' + Domainmodel2xsd.CR + comments + Domainmodel2xsd.CR +
			'<xs:complexType>' + Domainmodel2xsd.CR +
			'<xs:sequence>' + Domainmodel2xsd.CR +
			'<xs:element' +
			(if isOptional is true then ' minOccurs="0"' else "") +
			(if isMany is true then ' maxOccurs="unbounded"' else "") +
			' ref="' + prefix + ':' + type + '"' +
			' />' + Domainmodel2xsd.CR +
			'</xs:sequence>' + Domainmodel2xsd.CR +
			'</xs:complexType>' + Domainmodel2xsd.CR +
			'</xs:element>' + Domainmodel2xsd.CR, 3)
	else if prefix
		return Domainmodel2xsd.element(comments, name, prefix + ':' + type,
			isOptional, isMany, defaultValue, len, isInstance , pattern, range, enums)
	else if (type in Domainmodel2xsd.xsdComplexTypes or type in Domainmodel2xsd.xsdSimpleTypes)
		return Domainmodel2xsd.element(comments, name, 'xs:' + Domainmodel2xsd.toXsdType(type),
			isOptional, isMany, defaultValue, len, isInstance, pattern, range, enums)
	else
		return Domainmodel2xsd.element(comments, name, Domainmodel2xsd.localNamespace + ':' + type,
			isOptional, isMany, defaultValue, len, isInstance , pattern, range, enums)

Domainmodel2xsd.entityFeatureChoices = (entityFeature) ->
	choices = for x in entityFeature.types when x
		Domainmodel2xsd.entityFeatureChoice(x['type'], x['prefix'], x['isInstance'] is true)

	name = entityFeature.name
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	return Domainmodel2xsd.indent('<xs:element name="' + name + '"' +
		(if isOptional is true then ' minOccurs="0"' else "") +
		(if isMany is true then ' maxOccurs="unbounded"' else "") +
		(if defaultValue then ' default="' + defaultValue + '"' else "") +
		' >', 3) + Domainmodel2xsd.CR +
		Domainmodel2xsd.indent('<xs:complexType>',4) + Domainmodel2xsd.CR +
		Domainmodel2xsd.indent('<xs:choice>',5) + Domainmodel2xsd.CR +
		choices.join('') +
		Domainmodel2xsd.indent('</xs:choice>',5) + Domainmodel2xsd.CR +
		Domainmodel2xsd.indent('</xs:complexType>',4) + Domainmodel2xsd.CR +
		Domainmodel2xsd.indent('</xs:element>', 3) + Domainmodel2xsd.CR

Domainmodel2xsd.entityFeatureChoice = (type, prefix, isInstance) ->
	if type is '*'
		return Domainmodel2xsd.indent('<xs:element name="any" type="xs:anyType" />' + Domainmodel2xsd.CR, 6)
	else if prefix and isInstance is true
		return Domainmodel2xsd.indent('<xs:element name="' + type + '" >' + Domainmodel2xsd.CR +
			'<xs:complexType>' + Domainmodel2xsd.CR +
			'<xs:sequence>' + Domainmodel2xsd.CR +
			'<xs:element' +
			' ref="' + prefix + ':' + type + '"' +
			' />' + Domainmodel2xsd.CR +
			'</xs:sequence>' + Domainmodel2xsd.CR +
			'</xs:complexType>' + Domainmodel2xsd.CR +
			'</xs:element>' + Domainmodel2xsd.CR, 6)
	else if prefix
		return Domainmodel2xsd.element('', type, prefix + ':' + type,
			false, false, null, null, isInstance , null, null, [])
	else if type in Domainmodel2xsd.xsdComplexTypes or type in Domainmodel2xsd.xsdSimpleTypes
		return Domainmodel2xsd.element('', type, 'xs:' + Domainmodel2xsd.toXsdType(type),
			false, false, null, null, isInstance , null, null, [])
	else
		return Domainmodel2xsd.element('', Domainmodel2xsd.toFirstLower(type), Domainmodel2xsd.localNamespace + ':' + type,
			false, false, null, null, isInstance , null, null, [])

Domainmodel2xsd.entityFeatureAttribute = (entityFeature) ->
	name = entityFeature.name
	type = entityFeature.types[0]['type']
	isOptional = entityFeature.options.isOptional is true
	isMany = entityFeature.options.isMany is true
	defaultValue = entityFeature.options.defaultValue or ''
	len = entityFeature.options.len or 0
	pattern = entityFeature.options.pattern or ''
	range = entityFeature.options.range or {}
	enums = entityFeature.options.enums or []
	comments = entityFeature.comments or ''

	if len or pattern or enums.length or range.min
		return Domainmodel2xsd.indent('<xs:attribute name="' + name + '"' +
			(if not isOptional is true then ' use="required"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' >', 2) + Domainmodel2xsd.CR + comments + Domainmodel2xsd.CR +
			Domainmodel2xsd.indent('<xs:simpleType>', 3) + Domainmodel2xsd.CR +
			Domainmodel2xsd.restrictions('xs:' + Domainmodel2xsd.toXsdType(type), len, pattern, range, enums, 4) +
			Domainmodel2xsd.indent('</xs:simpleType>', 3) + Domainmodel2xsd.CR +
			Domainmodel2xsd.indent('</xs:attribute>', 2) + Domainmodel2xsd.CR
	else
		return Domainmodel2xsd.indent('<xs:attribute name="' + name + '"' +
			(if not isOptional is true then ' use="required"' else "") +
			(if defaultValue then ' default="' + defaultValue + '"' else "") +
			' type="xs:' + Domainmodel2xsd.toXsdType(type) + '"' +
			' >' + Domainmodel2xsd.CR + comments + Domainmodel2xsd.CR + '</xs:attribute>', 2)

Domainmodel2xsd.simpleEntity = (comments, name, type, prefix, options, annotations) ->
	options.len = 0 if not options.len
	options.pattern = '' if not options.pattern
	options.range = {} if not options.range
	options.enums = [] if not options.enums

	dataType = if prefix then prefix + ':' + type else type or "xs:string"
	dataType = 'xs:' + Domainmodel2xsd.toXsdType(type) if not prefix and (type in Domainmodel2xsd.xsdSimpleTypes or type in Domainmodel2xsd.xsdComplexTypes)
	option = Domainmodel2xsd.toOneSet(options)

	return { name: name, code: Domainmodel2xsd.indent(comments + '<xs:simpleType name="' + name + '" >', 1) + Domainmodel2xsd.CR +
		Domainmodel2xsd.restrictions(dataType, option.len, option.pattern, option.range, option.enums, 2) +
		Domainmodel2xsd.indent('</xs:simpleType>', 1) + Domainmodel2xsd.CR
	}

Domainmodel2xsd.restrictions = (type, len, pattern, range, enums, indent) ->
	enums = [] if not enums
	enumerations = for x in enums
		Domainmodel2xsd.indent('<xs:enumeration value="' + x + '"/>', indent + 1) + Domainmodel2xsd.CR

	return Domainmodel2xsd.indent('<xs:restriction base="'+ type + '">', indent) + Domainmodel2xsd.CR +
		(if pattern then Domainmodel2xsd.indent('<xs:pattern value="' + pattern + '" />' + Domainmodel2xsd.CR , indent + 1) else '') +
		(if len then Domainmodel2xsd.indent('<xs:maxLength value="' + len + '" />' + Domainmodel2xsd.CR , indent + 1) else '') +
		(if range and range.min then Domainmodel2xsd.indent('<xs:minInclusive value="' + range.min + '" />' + Domainmodel2xsd.CR , indent + 1) else '') +
		(if range and range.min then Domainmodel2xsd.indent('<xs:maxInclusive value="' + range.max + '" />' + Domainmodel2xsd.CR , indent + 1) else '') +
		(if enumerations and enumerations.length then enumerations.join('') else '') +
		Domainmodel2xsd.indent('</xs:restriction>', indent) + Domainmodel2xsd.CR

Domainmodel2xsd.range = (min, max) ->
	return { range: { min: min, max: max } }

Domainmodel2xsd.regex = (pattern) ->
	return { pattern: pattern }

Domainmodel2xsd.length = (len) ->
	return { len: len }

Domainmodel2xsd.enumeration = (enums) ->
	return { enums: enums }

Domainmodel2xsd.complexEntityFeatures = (entityFeatures) ->
	return entityFeatures

Domainmodel2xsd.isMany = () ->
	return { isMany: true }

Domainmodel2xsd.isOptional = () ->
	return { isOptional: true }

Domainmodel2xsd.unit = (unit) ->
	return { unit: unit }

Domainmodel2xsd.defaultValue = (value) ->
	return { defaultValue: value }

Domainmodel2xsd.entityFeature = ( comments, name, types, entityFeatureOptions, annotations ) ->
	return { name: name, comments: comments, types: types, options: Domainmodel2xsd.toOneSet(entityFeatureOptions), annotations: annotations }
