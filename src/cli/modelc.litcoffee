### Command Line Interface for P23R selection compiler

Author Jan Gottschick

#### Imports

Beside the standard libraries to parse a cli and accessing files the parser and its contained
generators for documentation in markdown and literate view as well for the xquery code are
required.

	fs = require 'fs'
	cli = require 'cli'

	try
		model = require '../../lib/js/model'
	catch error
		cli.fatal error.name + " importing model " + error.message

#### command line options

The library cli is used to parse the command line options. The library is also used to show all error
messages and information to the user.

	version = 0.1

	cli.enable('version','status')
	   .setApp('modelc', version)
	   .setUsage("modelc [OPTIONS] [infile [outfile]]\n\n" +
			"Compile P23R models to various target codes.")

	cli.argv.shift() if cli.argv[0]?.indexOf("modelc") > 0

	options = cli.parse({
		suffix: ['s', 'add suffix to target namespace of xsd schema', 'string'],
		untyped: ['u', 'generate untyped schema']
	}, ['xsd', 'json2xml','doc'])

The tool modelc translates P23R model source files into various codes.
The model compiler supports two arguments.

The argument inFile is the input file to parse.
The argument outFile is optional and is the output file which contains the result. If no output file
is given the result will be printed on the console.

Read and check the given arguments at the command line.

	cli.debug "fs: " + JSON.stringify(fs)

	inFile = if cli.args.length then cli.args.shift() else ''
	outFile = if cli.args.length then cli.args.shift() else ''

Get the standard input stream or an input file stream

	cli.debug "infile: " + inFile
	cli.debug "outFile: " + outFile
	try
		s = if inFile then fs.createReadStream(inFile, {encoding:'utf8'}) else process.stdin
	catch error
		cli.error "Cannot load inFile: #{inFile}"
		cli.fatal error

Read the input stream

	data = ''
	s.on 'data', (chunk)->
			data += chunk

Create the output code.

	s.on 'end', ->
		try
			if cli.command == 'xsd' and not options.untyped?
				cli.debug "create xsd code..."
				output = model.parse(data)
			else if cli.command == 'xsd' and options.untyped?
				cli.debug "create untyped xsd code..."
				output = model.parse(data, {startRule: 'Start4'})
			else if cli.command == 'json2xml'
				cli.debug "create json to xml coffeescript code..."
				output = model.parse(data, {startRule: 'Start3'})
			else if cli.command == 'doc'
				cli.debug "create literate documentation..."
				output = model.parse(data, {startRule: 'Start2'})
			cli.debug "get resulting output code\n" + output
		catch error
			cli.fatal error.name + " in model at " + error.line + "," + error.column + ": " + error.message

Add suffix to the namespace if given.

		output = output.replace(/<xs:schema .+ targetNamespace="[^"]+/, '$&' + options.suffix) if options.suffix

Write the result back to a given file or the console.

		cli.debug "writing result"
		if outFile && output
			try
				fs.writeFileSync(outFile, output, 'utf8')
			catch error
				cli.fatal  "Cannot write outFile: #{outFile}"
		else if output
			console.log output
