# README.md

Author: Jan Gottschick <service@openp23r.org>

This is a P23R model language to XSD schema compiler. P23R model
is internally used in the openP23R project to describe schemata but is **not**
part of the official P23R specifications.

https://entwickler.p23r.de


## Usage as a command

The compiler is implemented in Javascript and requires *node.js*, which is available
at

http://nodejs.org

After installing node.js you can install the p23r selection compiler, globally, by

	npm install p23r-model-compiler

and start the compiler by

	modelc

You can enter the p23r model source code via stdin and the target code will
appear on stdout, e.g. XSD

	cat | modelc xsd
	This model is a very simple example

	model simple as s release 1.0 {

		creator 'Jan Gottschick'
		organisation 'P23R-Team'
		base http://example.de/NS/
		requires p23r release 1.1

		title 'simple example'

		description 'This model is used by the README file.'

		entity RootTag {
			id : string
		}
	}

will output

	<?xml version="1.0" encoding="utf-8" ?>
	<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://example.de/NS/Simple1-0" xmlns:s="http://example.de/NS/Simple1-0" elementFormDefault="qualified" version="1.0" >
		<xs:annotation>
			<xs:documentation>
				This model is a very simple example
			</xs:documentation>
		</xs:annotation>

		<xs:complexType name="RootTag"  >
			<xs:attribute name="id" use="required" type="xs:string" >
			</xs:attribute>
		</xs:complexType>
	</xs:schema>

The following options are available:

	modelc [OPTIONS] [infile [outfile]]

	Compile P23R models to various target codes.

	Options:

		-s, --suffix [STRING]  add suffix to target namespace of xsd schema (Default is suffix)
		-u, --untyped          generate untyped schema
		-k, --no-color         Omit color from output
		--debug            Show debug information
		-v, --version          Display the current version
		-h, --help             Display help and usage details

	Commands:
		xsd

You must include a command as part of the options to specify which
type of code should be generated.

## Usage inside your code

You can include the p23r model compiler in your project, too. If you
build a server side service using node.js just include `p23r-model-compiler`
in your `package.json` file. You can call the command using a relative path
to `node_modules/p23r-model-compiler/modelc`. Or you use the library
the usual way, e.g. in coffeescript

	model = require 'p23r-model-compiler'

	...
	modelTargetCode = model.model2xsd(modelSourceCode)

If you like to use the compiler inside your browser code you can load
the p23r model compiler by using bower. Just include `p23r-model-compiler`
in your `bower.json` file. Then load the files

* ../p23r-model-compiler/dist/lib/js/modelLib.js
* ../p23r-model-compiler/dist/lib/js/model2xsd.js

with your web page and call the parser

	modelTargetCode = model.parse(modelSourceCode)

Errors can be handled in both cases using try and catch, e.g.

	try
		modelTargetCode = model.parse(modelSourceCode)
	catch error
		console.log error.name + " in model at " + error.line + "," + error.column + ": " + error.message

## Legal issues

This project is part of the *openP23R* initiative. All related projects
are listed at https://gitlab.com/openp23r/openp23r .

The legal conditions see *LICENSE* file.

The project is maintained by P23R-Team (a) Fraunhofer FOKUS
