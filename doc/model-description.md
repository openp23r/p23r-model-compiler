# Informelle Beschreibung von P23R Model

<small>Version: 1.0 by P23R-Team</small>

Um Benachrichtigungsregeln für einen P23R-Prozessor zu implementieren, müssen verschiedenste Datenmodelle definiert werden, beispielsweise das Datenformat für die eingehende Nachricht, die Datenmodelle der Quelldatenkonnektoren, temporäre Datenformate, die während der Generierung als Zwischenergebnisse benötigt werden, und das Datenformat der zu versendenden Benachrichtigung. Da die Daten beim P23R grundsätzlich in XML vorliegen und bearbeitet werden, bietet sich die Sprache _XML Schema Definition_ (XSD) der W3C an. Im Rahmen der _Technischen Benachrichtigungsregelsprache_ (T-BRS) müssen Datenmodelle als XSD-Dateien in Benachrichtigungsregelgruppen und den Datenmodellpaketen dem P23R-Prozessor bereitgestellt werden.

### Motivation

XSD-Dateien können in einem Texteditor erstellt werden. Dafür sind am Markt zahlreiche grafischen Editoren verfügbar, die zur Erstellung und Pflege der Datenmodelle im Kontext P23R genutzt werden können. XSD ist zwar sehr mächtig und flexibel, anderseits jedoch schwer lesbar und vielfach zu komplex. Um die Komplexität zu verringern, die Lesbarkeit und Verständlichkeit zu verbessern, die Einheitlichkeit der Datenmodelle zu fördern, das direkte Editieren im P23R-Entwicklerportal zu ermöglichen, die Erstellung der Dokumentation zu erleichtern und eine langfristige Wartung der Datenmodelle sicherzustellen, bietet das P23R-Entwicklerportal alternativ die _Domain Specific Language_ (DSL) _P23R Model_ an. P23R Model ist Teil der fachlichen Beschreibungssprachen (F-BRS), die das Fraunhofer FOKUS zur einfacheren Erstellung von P23R-Benachrichtigungsregeln und Pivot-Teildatenmodellen entwickelt und anbietet. Dabei ist zu beachten, dass jedes valide P23R Model in XSD übersetzt werden kann. Aber nicht jedes XML Schema ist mit P23R Model beschreibbar. Dies ist gewollt. Die Datenmodelle werden im Interesse der Einheitlichkeit und zur Umsetzung Leitstellen-spezifischer Vorgaben (Leitlinien) gezielt eingeschränkt.

### Grundsätzliches

P23R Models sind einfache Textdateien, die mit jedem Texteditor editiert werden können. Die Grammatik orientiert sich im Wesentlichen an den Schreibweisen von gängigen Programmiersprachen. Die Definition der Datenmodelle in P23R Model mit seinen Entitäten und ihren Eigenschaften ist direkt vergleichba mit der Definition von Typen in statisch getypten Programmiersprachen. Das P23R-Entwicklerportal bietet einen integrierten Konverter, der die P23R Models automatisch in XSD-Dateien übersetzt. Nachfolgend wird beispielhaft beschrieben, wie die P23R Models formal aufgebaut sind und wie die Datenmodelle zu beschreiben sind. Eine formale Grammatik für P23R Model ist ebenfalls im Info-Bereich des P23R-Entwicklerportals  verfügbar.

Die Sprache der P23R Models basiert auf der Grundidee des *Literate Programming*, um die Datenmodelle angemessen zu kommentieren bzw. um in die informelle Beschreibung der Datenmodelle um die notwendige formale Spezifikation zu ergänzen. Entsprechend ist die Basis von P23R Models ein reiner Text im Textformat *Markdown*, das die einfache Auszeichnung und Formatierung von Textteilen erlaubt, beispielsweise für Hervorhebungen, Absätze, Listen, Verweise etc. In diesem informellen Text, d. h. der Dokumentation, kann man direkt Quellcode einbetten. Der Dokumentationstext muss immer am Zeilenbeginn starten bzw. kann einfache Leerzeichen für Einrückungen verwenden. Der Quellcode-Teil zur formalen Spezifikation der Datenmodelle muss dagegen immer vom Zeilenanfang mindestens mittels eines Tab (Tabulator) eingerückt werden.

Die nachfolgenden Beispiele dieser Beschreibung zeigen als Quellcode nur den reinen Quellcode zur Spezifikation der Datenmodelle. Er ist nicht zusätzlich eingerückt und ohne Kommentare, diese Beschreibung bereits die Dokumentation zum gezeigten Quellcode darstellt. Wie man mittels Markdown die Dokumentation auszeichnet, wird ebenfalls nicht erläutert. Hier wird auf die Originaldokumentation von [Markdown](http://daringfireball.net/projects/markdown/syntax "Markdown") verwiesen.

### Konventionen

Bei der Beschreibung der Grammatik wurden folgende Konventionen verwendet:

* Ein Element muss genau einmal vorkommen, wenn es als verpflichtend mit *MUSS* beschrieben ist.
* Ein Element kann genau einmal vorkommen, wenn es als optional mit *KANN* beschrieben ist.
* Ein Element muss mindestens einmal und kann mehrfach vorkommen, wenn es als *MEHRFACH* beschrieben ist.
* Platzhalter werden in spitzen Klammern eingeschlossen (*«* und *»*).
* Inhalte werden mit *#«Buchstabe»* oder *← #«Buchstabe»* referenziert, d. h. sind nicht Teil der Grammatik.

### Metainformationen

Der äußere Rahmen einer P23R-Model-Datei beinhaltet den vollqualifizierten Namen des Datenmodells (#A), die bevorzugte Kurzbezeichnung (#B) und die Releasenummer (#C) für die Veröffentlichung.

	model «#A» as «#B» release «#C» {
		«...»
	}

beispielsweise

	model de.firma.adressBuch.kontakt as ko release 1.0 {
		<...>
	}

Am Anfang des Datenmodells müssen zunächst Metainformationen in einer vordefinierten Reihenfolge angegeben werden, die bei Bedarf zur Verwaltung der Datenmodelle in einem Repository u. Ä. genutzt werden können:

* Die Angabe des verantwortlichen Autors des Dokumentes (#A) ist verpflichtend.
* Optional sollte die für die Pflege des Dokumentes verpflichtende Organisation (#B) angegeben werden.
* Optional sollte ein Präfix (#C) für die URL des Namespaces für das Schema angegeben werden, wobei dieser Präfix später beim Generieren des Schemas um den qualifizierten Namen unter Beachtung der zulässigen Schreibweise ergänzt wird, beispielsweise *http://www.firma.de/NS/de/firma/addressbuch/kontakt*.  Der Standardwert ist der Präfix der Leitstelle *http://leitstelle.p23r.de/NS* und ist für Schema reserviert, für die die Leitstelle die Pflege sicherstellt.
* Optional und mehrfach können Abhängigkeiten von anderen Datenmodellen (#D)  explizit angegeben werden, wobei auf deren qualifizierten Namen und ihrer veröffentlichten Releasenummer verwiesen wird.
* Optional kann angegeben werden, für welche maximale Releasenummer (#E) der P23R-Spezifikationen das Schema aus diesem Datenmodell erzeugt werden kann, d. h. mit welchen Releases es kompatibel ist.
* Der verpflichtend anzugebende Titel (#F) beschreibt eindeutig das Ziel und den Zweck des Datenmodells.
* Optional können mehrfach Schlagwörter (#G) zur Kategorisierung des Datenmodells angegeben werden, beispielsweise um die Suche nach dem Datenmodell im P23R-Entwicklerportal zu systematisieren und zu vereinfachen.
* Die verpflichtend anzugebende Beschreibung (#H) muss den Hintergrund für das Datenmodell ausreichend erläutern. Beispielsweise sollte es die Fragen beantworten:
  * Warum und wofür wurde dieses Datenmodell entworfen?
  * In welchem Kontext und Hintergrund wird dieses Modell angewendet?
  * Welchen Nutzen hat der Anwender?

Daraus ergeben sich folgende Metainformationen für das Beispiel:

	model de.firma.adressBuch.kontakt as ko release 1.0 {
		creator 'P23R core team' ← #A
		organisation 'Öffentliche P23R Leitstelle' ← #B
		base http://www.firma.de/NS/ ← #C
		depends on de.firma.common release 0.2 ← #D
		requires p23r release 1.0 ← #E
		title 'Kontaktdaten des Adressbuches' ← #F
		tag 'Kontakt' ← #G
		tag 'Adressbuch'
		description 'Das Datenmodell beschreibt einen Kontakt, wie er im Adressbuch abgelegt wird.' ← #H
		«...»
	}

### Benötigte Namensräume

Um Typen bestehender Schemata bei der Definition des Datenmodells in P23R Model verwenden zu können, können deren Namensräume optional bekannt gemacht werden. Dazu muss die entsprechende URI (#B) einem Kurzbezeichner (#A) zugewiesen werden. Der Kurzbezeichner kann später als Präfix genutzt werden, um auf die Typen des Schematas verweisen zu können. Optional sollte noch der Dateiname (#C) angegeben werden, wo sich das Schema als lokale Kopie befindet.

	namespace «#B»=«#A» file «#C»

beispielsweise

	«...»
	namespace cc=http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0 file 'Common.xsd'
	«...»

Der Namespace der W3C wird automatisch bei jeder Model Datei importiert und muss daher nicht eingetragen werden. Die Typen sind standardmäßig vorhanden und werden ohne Präfix referenziert.

### Entitäten

Der Kern von P23R Model ist die Definition von Entitäten. Jede Entität hat verpflichtend einen Namen (#A). Eine Entität kann wie ein komplexer Datentyp genutzt werden, beispielsweise um ein XML-Element zu erzeugen. Eine Entität kann optional einen Inhalt (#B) haben, der optional getypt sein kann. Außerdem kann eine Entität optional mehrfach Eigenschaften (#C) definieren, die jeweils verpflichtend einen Typ (#C) haben müssen. So sieht die Struktur einer Entität aus:

	entity «#A»: «#B» {
		«#C» : «#D»
	}

Die Bezeichner von Entitäten sollten grundsätzlich mit einem Großbuchstaben beginnen und den Schreibkonventionen für _Upper Camel Case_ folgen. Die Bezeichner von Eigenschaften sollten dagegen mit einem Kleinbuchstaben anfangen und den Schreibkonventionen für _Lower Camel Case_ folgen. Sollte das Zielformat unterschiedliche Namenskonventionen haben, beispielsweise werden Tags in XML immer kleingeschrieben, sind die Bezeichner im entsprechenden Konverter automatisch anzupassen.

Beispielsweise wird die Entität _Person_ mit den Eigenschaften *vorname*, *nachname* und _notizen_ beschrieben. Der Typ der Eigenschaft _notizen_ ist selbst eine Entität die als Wert beliebigen Inhalt (Text, Zahlen, ...) haben kann, und außerdem die Eigenschaft *geaendertAm* mit dem letzten Änderungsdatum besitzt.

	entity Person : {
		vorname : string
		nachname : string
		notizen : Text
	}

	entity Text : * {
		geaendertAm: date
	}

### Typen

Der Typ des Inhalts einer Entität oder der Eigenschaft kann ein vordefinierter Typ, der Name einer selbst definierten Entität oder eine externe Typdefinition sein.

Die Standardtypen der [W3C](http://www.w3.org/TR/xmlschema11-2/ "W3C"), die für Entitäten und Eigenschaften als vordefinierte Typen genutzt werden können, umfassen:

* string
* boolean
* decimal
* float
* hexBinary
* base64Binary
* anyURI
* qName
* language
* name
* id
* integer
* nonPositiveInteger
* negativeInteger
* nonNegativeInteger
* positiveInteger
* duration
* dateTime
* time
* date
* gYearMonth
* gYear
* gMonthDay
* gDay
* gMonth
* yearMonthDuration
* dayTimeDuration
* text

Standardmäßig werden Eigenschaften, die vordefinierte Typen verwenden, als einfache Datentypen umgesetzt. So werden diese beispielsweise bei der Konvertierung von P23R Model in ein XSD-Schema als XML-Attribute realisiert, soweit deren Multiplizität und Restriktionen nicht die Umsetzung als XML-Element erzwingt. Wenn man Eigenschaften, die vordefinierte Typen verwenden, auf jeden Fall als komplexen Datentyp umsetzen will, beispielsweise damit die Konvertierung in ein XSD-Schema die Eigenschaft immer als XML-Element realisiert, muss der entsprechende Standardtyp mit einem Großbuchstaben am Anfang geschrieben werden, beispielsweise statt _string_ schreibt man dann _String_.

Durch die optionale Angabe von Namensräumen (siehe oben) können die für den Namensraum definierten Typen verwendet werden. Dazu wird der für den Namensraum definierte Kurzbezeichner als Präfix dem Typ vorangestellt.

	namespace cc=http://leitstelle.p23r.de/NS/p23r/nrl/Common1-0 file 'Common.xsd'
	«...»
	release : cc:Release

Damit können Datenmodelle modularisiert werden. Dabei kann das Datenmodell, das durch den externen Namensraum gegeben ist, sowohl ein weiteres P23R Model als auch ein XSD-Schema sein. Wenn in dem externen Datenmodell ein _Element_ als Referenz  statt ein Typ genutzt werden soll, muss dies durch die Schlüsselwörter _instance of_ angezeigt werden.

	namespace ds=http://www.w3.org/2000/09/xmldsig# file 'xmldsig-core-schema.xsd'
	«...»
	entity Container {
		content : Content
		contentSignature : instance of ds:Signature
	}

Darüber hinaus kann optional als Typ `*` bei Entitäten angegeben werden, wobei dann als Inhalt der Entität alles akzeptiert wird, d. h. auch nicht aufgeführte Eigenschaften.

Die Typen von Entitäten und Eigenschaften können neben der Angabe eines vordefinierten Typs oder einer Entität als Typ optional noch präzisiert werden, beispielsweise um die zulässigen Werte bei vordefinierten Typen einzuschränken oder die Multiplizitäten anzugeben.

### Multiplizitäten

Standardmäßig muss eine Eigenschaft verpflichtend in einer Instanz angegeben werden. Mit dem Schlüsselwort _optional_ kann die Eigenschaft optional in einer Instanz weggelassen werden.

	geburtstag : date optional

Eine Eigenschaft, bei der das Schlüsselwort _many_ angegeben wurde, kann optional einmal oder mehrfach vorkommen. Die Schlüsselwörter _optional_ und _many_ können auch kombiniert werden, so dass eine Eigenschaft optional gar nicht oder mehrfach vorkommen kann.

	arbeitsorte: Adresse optional many

Wenn das Schlüsselwort _many_ angegeben wird, sollte der Name einer Eigenschaft immer als Pluralform angegeben werden, um Mengen von Werten sofort zu erkennen und vor allem weil bei einer Übersetzung von Modellen in eine Bibliothek für eine typische Programmiersprache Iterationen leichter lesbarer sind (`foreach x in arbeitsorte`).

### Restriktionen

Um die Länge von Zeichenketten u.Ä. bei Inhalten von Entitäten und bei Eigenschaften zu beschränken, kann optional das Schlüsselwort _length_ gefolgt von einer positiven, natürlichen Zahl angegeben werden.

	strasse : string length 50

Um Zeichenketten bei Inhalten von Entitäten und bei Eigenschaften auf vorgegebene Werte zu beschränken, kann optional eine Aufzählung der zulässigen Werte erfolgen.

	anrede : string from "Herr" "Frau"

Eine Alternative um Zeichenketten einzuschränken ist die optionale Angabe eines Textmusters, auf das der Wert des Inhalts einer Entität oder der Eigenschaft passen muss. Das Textmuster wird technisch durch _Reguläre Ausdrücke_ innerhalb von Schrägstrichen ausgedrückt.

	telefonNr : string match /^([+]?\d+)[ -]([1-9]\d+)[ -]([1-9]\d+)([-]\d+)?$/

Bei Zahlen kann optional der Wertebereich beim Inhalt einer Entität oder bei einer Eigenschaft durch eine untere und obere Grenze eingeschränkt werden.

	plz : int range 1000 to 9999

Wenn die Instanz des Inhalts einer Entität oder der Eigenschaft standardmäßig einen Wert enthalten soll, kann optional ein Standardwert für den Inhalt einer Entität oder für eine Eigenschaft angegeben werden.

	ort : string default "Berlin"

Bei Zahlen kann optional ein Typ für die zulässigen Maßeinheiten angegeben werden.

	flaeche : float unit Meter

### Varianten

Wenn Inhalte von Eigenschaften bezüglich ihres Typs variieren können, kann man als Typ optional mehrere Varianten für den zu instanziierenden Typ angeben. Der tatsächlich instanziierte Typ kann dann zur Laufzeit ermittelt und ausgewertet werden.

	beschreibung: Richtext | Markdown | String

### Annotationen

Um Konvertern u.ä. Hinweise für die Generierung der konkreten Zielschemata zu geben, können Entitäten und Eigenschaften optional annotiert werden. Eine Annotation ist eine Liste von Schlüsselwörtern. Deren Bedeutung ist nicht vorgegeben und deren Umsetzung im Konverter ist implementierungsabhängig.

	[obsolete]
	entity Alt {
		[R1.0]
		wichtig : boolean
	}

### Beispiel

Nachfolgende ein kleines Beispiel für ein vollständiges P23R Model.

	model fbrs.p23rModel.beispiel.adressBuch as ab release 1.0 {
		creator 'P23R core team'
		organisation 'Öffentliche P23R Leitstelle'
		base http://entwickler.p23r.de/NS/
		requires p23r release 1.0
		title 'P23R Model Einsteigerbeispiel Adressbuch'
		tag 'Kontakt'
		tag 'Person'
		tag 'Adressbuch'
		description 'Das Datenmodell beschreibt ein Adressbuch mit Angaben zur Person und seine Adressen.'

		namespace ds=http://www.w3.org/2000/09/xmldsig# file 'xmldsig-core-schema.xsd'

		entity Adressbuch {
			eintraege : SignedEintrag optional many
		}

		entity SignedEintrag {
			eintrag : Eintrag
			eintragSignature : instance of ds:Signature
		}

		entity Eintrag {
			person: Person | Firma
			kontakte : Adresse optional many
			anmerkungen : Markdown | String
		}

		entity Person {
			vorname : string length 50
			nachname : string length 50
			titel : string from 'Dr.' 'Prof.' 'Prof. Dr.' 'Dr. med.' 'Phd.'
			geburtstag : date optional
			groesse : float unit Meter optional
			sprachen : string many match /^[a-z][a-z]-[a-z][a-z]$/
		}

		entity Firma {
			name : string length 100
			gesellschaftsForm : string from 'GbR' 'GmbH' 'AG' 'e.V.' 'eG'
		}

		entity Adresse {
			zusatz : string length 50 optional
			strasse : string length 50
			hausnummer : string length 10
			packstation : string match /^Packstation \d+$/
			ort : string length 50
			plz : int range 1000 to 9999
			land : string length 20
		}

		entity Markdown : string {
			lastUpdated : dateTime optional
		}
	}

Das obige P23R Model lässt sich grafisch als folgendes Diagramm darstellen.

<img src="./model.png" width="100%" />

Aus dem obigen P23R Model erzeugt der XSD-Konverter im P23R-Entwicklerportal das folgende XSD-Schema.

	<?xml version="1.0" encoding="utf-8" ?>
	<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" targetNamespace="http://entwickler.p23r.de/NS/fbrs/p23rModel/beispiel/AdressBuch1-0" xmlns:ab="http://entwickler.p23r.de/NS/fbrs/p23rModel/beispiel/AdressBuch1-0" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" elementFormDefault="qualified" version="1.0" >

	 <xs:import id="ds" namespace="http://www.w3.org/2000/09/xmldsig#" schemaLocation="xmldsig-core-schema.xsd" />

	 <xs:complexType name="Adressbuch"  >
	   <xs:sequence>
	     <xs:element name="eintraege" minOccurs="0" maxOccurs="unbounded" type="ab:SignedEintrag" />
	   </xs:sequence>
	 </xs:complexType>

	 <xs:complexType name="SignedEintrag"  >
	   <xs:sequence>
	     <xs:element name="eintrag" type="ab:Eintrag" />
	     <xs:element name="eintragSignature" >
	<xs:complexType>
	<xs:sequence>
	<xs:element ref="ds:Signature" />
	</xs:sequence>
	</xs:complexType>
	</xs:element>
	   </xs:sequence>
	 </xs:complexType>

	 <xs:complexType name="Eintrag"  >
	   <xs:sequence>
	     <xs:element name="person" type="ab:Person" />
	     <xs:element name="kontakte" minOccurs="0" maxOccurs="unbounded" type="ab:Adresse" />
	     <xs:element name="anmerkungen" type="ab:Markdown" />
	   </xs:sequence>
	 </xs:complexType>

	 <xs:complexType name="Person"  >
	   <xs:sequence>
	     <xs:element name="sprachen" maxOccurs="unbounded" >
	       <xs:simpleType>
	         <xs:restriction base="xs:string">
	           <xs:pattern value="^[a-z][a-z]-[a-z][a-z]$" />
	         </xs:restriction>
	       </xs:simpleType>
	     </xs:element>
	   </xs:sequence>
	   <xs:attribute name="vorname" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:maxLength value="50" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="nachname" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:maxLength value="50" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="titel" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:enumeration value="Dr."/>
	         <xs:enumeration value="Prof."/>
	         <xs:enumeration value="Prof. Dr."/>
	         <xs:enumeration value="Dr. med."/>
	         <xs:enumeration value="Phd."/>
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="geburtstag" type="xs:date" />
	 </xs:complexType>

	 <xs:complexType name="Adresse"  >
	   <xs:attribute name="zusatz" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:maxLength value="50" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="strasse" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:maxLength value="50" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="hausnummer" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:maxLength value="10" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="packstation" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:pattern value="^Packstation \d+$" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="ort" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:maxLength value="50" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="plz" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:int">
	         <xs:minInclusive value="1000" />
	         <xs:maxInclusive value="9999" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	   <xs:attribute name="land" use="required" >
	     <xs:simpleType>
	       <xs:restriction base="xs:string">
	         <xs:maxLength value="20" />
	       </xs:restriction>
	     </xs:simpleType>
	   </xs:attribute>
	 </xs:complexType>

	 <xs:complexType name="Markdown" mixed="true"  >
	   <xs:attribute name="lastUpdated" type="xs:dateTime" />
	 </xs:complexType>

	</xs:schema>

### Konventionen des model-to-xsd-Konverters

Die Umsetzung von P23R Model in eine Zielsprache wie XSD-Schema setzt
auch bestimmte Konventionen um, die durch die Leitstelle festgelegt werden.
Der model-to-xsd-Konverter des Entwicklerportals der Öffentlichen P23R-Leitstelle
setzt folgende Konventionen und Strategien um:

* Alle Entitäten sind komplexe Datentypen.
* Alle vordefinierten Datentypen, die mit einem Kleinbuchstaben anfangen, sind einfache Datentypen mit Ausnahme von base64Binary und text, die komplexe Datentypen sind.
* Alle vordefinierten Datentypen, die mit einem Großbuchstaben anfangen, sind komplexe Datentypen.
* Extern definierte Datentypen sind immer komplexe Datentypen.
* Eigenschaften mit einfachen Datentypen werden als XSD-Attribute umgesetzt, es sei denn sie können mehrfach vorkommen.
* Eigenschaften mit komplexen Datentypen werden als XSD-Elemente umgesetzt.
* Eigenschaften, deren Datentyp als Instanz markiert ist, werden als XSD-Elemente umgesetzt, deren Typ eine Referenz auf den eigentlichen Datentyp darstellt.
* Die Kommentare in P23R Model werden als XSD-Annotationen übernommen.
* Es dürfen nur P23R Model's den Namespace-Präfix *http://www.p23r.de/* oder *http://entwickler.p23r.de/* nutzen, die vom P23R-Team gepflegt werden.

Darüber hinaus kann es weitere Einschränkungen durch die Leitstelle geben, die im
model-to-xsd-Konverter nicht abgetestet werden, sehr wohl aber durch QS-Werkzeuge bei der Veröffentlichung:

* Vordefinierte Datentypen, die mit einem Großbuchstaben beginnen, sind aus Kompatibilitätsgründen in den interne Datenmodellen einer Benachrichtigungsregel zulässig.
* Vordefinierte Datentypen, die mit einem Großbuchstaben beginnen, sind in Pivot-Teildatenmodellen nicht zulässig, um eine Einheitlichkeit zu gewährleisten.

Diese Art von Einschränkungen führen dann zu Warnungen/Fehlern in den QS-Werkzeugen der Leitstelle und könnten eine Veröffentlichung einer Benachrichtigunsgregelgruppe oder vor allem eines Pivot-Teildatenmodells verhindern.

Die Maßeinheiten werden durch den model-to-xsd-Konverter nicht unterstützt.
