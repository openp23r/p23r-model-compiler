# P23R Model Grammar

<small>Version: 1.0 by P23R-Team</small>

The grammar is specified by using a *Parsing Expression Grammar* as described on *http://pegjs.majda.cz*.

P23R  Models defines entities with its properties and relations in a similar way as complex types are defined in typical programming languages.

P23R Models are based on the idea of literate programming. Therefore a P23R model file is always a markdown file. The markdown text represents the comments. The embedded code, which has to be indented by a tab, are the core of a model definition. This means your comments always starts at the beginning of a line or is indented only by normal spaces. The main part of the code describing the model has to be indented explicitly using a tab character.

A P23R Model definition could be followed up by a comment or only a comment without a model definition.

	P23rModel
		= Model EMPTYLINE* COMMENTS EMPTYLINE*
		/ COMMENTS EMPTYLINE*

## The Tokens

This section defines all kind of tokens required in the grammar.

### Comments and White Spaces

The white spaces must be parsed explicitely. The White spaces include the space and tab character as well as new line CR and LF characters. The white spaces mainly separate keywords, identifiers and numbers. The white spaces subsumes also new line character and followup empty lines.

	WSEOL
		= WS
		/ EOL TAB

	WS
		= [\t\v\f \u00A0\uFEFF]
	 / [\u0020\u00A0\u1680\u180E\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200A\u202F\u205F\u3000]

	__ "white space character"
		= WSEOL+

	_ "white space character"
		= WSEOL*

	TAB "tabulator"
		= "\t"

	EOL
		= "\n"
	 / "\r\n"
	 / "\r"
	 / "\u2028"
	 / "\u2029"

This rule subsumes an empty line. An empty line could contain spaces and tabs.

	EMPTYLINE "empty line"
		= WS* EOL

	COMMENTSTART
		= [^\t\r\n]

 A single comment line includes everything up to the end of the line.

	COMMENT
		= WS* [^\r\n]+ [\r\n]
		/ WS* [^\r\n]+

 A comment block includes all subsequent comment lines. A comment block could include empty lines, too, which are inside comment line.

	COMMENTBLOCK
		= COMMENTSTART COMMENT+ EMPTYLINE*

 Comments are a list of subsequent comment blocks, which are separated by empty lines and must end with an empty line, too.

	COMMENTS "comments"
		= COMMENTBLOCK*

### Basic Types

 A cardinal is a non negative integer and simply a sequence of digits.

	CARDINAL "cardinal"
		= [0-9]+

	NUMBER "number"
		= CARDINAL

 A string must be pairwise surrounded by *"* or *'* characters. A string could contain any characters except the surrounding character. A string must be written within a line.

	STRING "string"
		= '"' [^"\n\r]* '"'
		/ "'" [^'\n\r]* "'"

 A multi language string must be prefixed by the language code, which specifies the language of the string. The language code must be enclosed by square brackets. The language codes are predefined by ISO639-1.

	MLSTRING
		= "[" LC "]" WS* STRING
		/ "[" LC "]" WS* STRING
		/ STRING

	LC "language code"
		= ([a-z][a-z])

 A regular expression must be pairwise surrounded by */* characters. A regular expression could contain any characters except the surrounding character. A regular expression must be written within a line.

	REGEX "regex"
		= '/' [^\/\n\r]* '/'


### Identifiers

This is the atomic identifier, which is reused by other rules. The atomic identifier must start with a letter and could contain letters, digits as well as the underline and hyphen character. Therefore it follows the rules of popular programming languages.

	Identifier
		= [a-zA-Z] [a-zA-Z0-9_-]*

	UccIdentifier
		= [A-Z] [a-zA-Z0-9]*

A package name includes the path for a qualified identifier and will be constructed by concatenating a sequence of atomic identifier with a dot.

	Package "package name"
		= PID*

	PID
		= Identifier "."

This is a basic identifier, which contains no context/path. It follows the definition of the atomic identifier.

	ID "identifier"
		= Identifier

	UCCID "ucc identifier"
		= UccIdentifier

A qualified identifier must be prefixed by a package name. It follows the rules of popular programming languages. Optionally a qualified identifier can have an wildcard character as the name.

	QualifiedID "qualified identifier"
		= Package ID

	QualifiedIDWithWildcard "qualified identifier with wildcard"
		= Package ID
		/ Package "*"

### URLs

A URL follows basically the syntax rules as defined by RFC 3986. It is restricted to web based URLs.

	URL
		= PROTOCOL
			host:HOST
			port:PORT?
			path:PATH?

	PROTOCOL
		= "i
		/ "i

	HOST "host name"
		= DOMAINID PARTIALDOMAIN+
		/ CARDINAL PARTIALIPNUMBER+

	PARTIALIPNUMBER
		= "." CARDINAL

	PARTIALDOMAIN
		= "." DOMAINID

	DOMAINID
		= [a-zA-Z] [a-zA-Z0-9-]*

	PORT "port"
		= " CARDINAL

	PATH "local path"
		= "/" FILE PARTIALPATH?
		/ "/" HASH?

	PARTIALPATH
		= "/" FILE PARTIALPATH?
		/ "/" HASH?
		/ HASH

	HASH "hash part"
		= "#" [a-zA-Z0-9_-]*

	FILE
		= [a-zA-Z0-9_-]* "." [a-zA-Z0-9_-]*
		/ [a-zA-Z0-9_-]*

### Release Numbers

 A release number consists of a major number and could optionally extended by a minor number.

	RELEASE "release number"
		= CARDINAL "." CARDINAL
		/ CARDINAL

### Model

This rule parses the basic structure of the model. A P23R model is a self-contained module. The key identifiers are its name and actual release number. It contains various a shortcut, the meta information and the entities.

	Model
		= COMMENTS
			TAB _
			annotations:Annotations
			_ "model"i __ ModelName __
			_ "as"i __ ModelNS __
			"release"i __ RELEASE _
			"{" EMPTYLINE+
				MetaInformation
				ExternalType*
				Entity*
				COMMENTS
			TAB _ "}"

	ModelName "model identifier"
		= QualifiedID

### Meta Information

The meta information contain administrative information as well as documentation, as the author of the model, dependencies to other models, the supported release of p23r specification, the full title of the model, tags to categorize a model using a controlled vocabulary e.g. for search, a description of the model to explain the background of the model and its remarkable aspects, and a definition of the target domain e.g. where and when the model should be used.

	MetaInformation
		= Creator
			Organisation?
			Base?
			Dependency*
			Specification?
			Title
			Tag*
			Description
			Target*

The author of a model.

	Creator
		= TAB _ "creator"i __ STRING EMPTYLINE+

The organisation, which maintains of the model.

	Organisation
		= TAB _ "organisation"i __ STRING EMPTYLINE+
		/ TAB _ "organization"i __ STRING EMPTYLINE+  

	Base
		= TAB _ "base"i __ URL EMPTYLINE+

	Dependency
		= TAB _ "depends"i __ "on"i __ ID __ "release"i __ RELEASE EMPTYLINE+

Defines on which version of the p23r specification this grammar depends.

	Specification
		= TAB _ "requires"i __ "p23r"i __ "release"i __ RELEASE EMPTYLINE+

A descriptive, longer title for the model.

	Title
		= TAB _ "title"i __ MLSTRING EMPTYLINE+

The key words, which best describe the content of the model, e.g. for search.

	Tag
		= TAB _ "tag"i __ STRING EMPTYLINE+

A description about the usages und background of the model.

	Description
		= TAB _ "description"i __ MLSTRING EMPTYLINE+

	Target
		= TAB _ "target"i __ MLSTRING EMPTYLINE+

 ### Model Content

Declaring external type definitions, so their types can be used as predefined types in the model by prefixing them with the declared namespace id.

	ExternalType
		= COMMENTS?
	   TAB _ "namespace"i
			__ ID
			_ "="
			_ URL
			ExternalTypeFile?
			EMPTYLINE+

	ExternalTypeFile
		= __ "file"i __ STRING

Define a new entity, which could by used as a type. An entity has a type identifier, embedded content, and properties (features). The annotations kann be used as meta information, e.g. to control special aspects of the code generations.

	Entity
	 = COMMENTS?
	   TAB _
			annotations:Annotations
			_ "entity"i __ UCCID
			EntityType?
			EntityOption*
			EntityFeatures?
			EMPTYLINE+

	EntityFeatures
		= _ "{"
			EMPTYLINE+
			Feature*
			TAB _ "}"

Each feature describes a property of the entity in detail.

	Feature
		= COMMENTS?
	   TAB _
			annotations:Annotations
			_ ID _ ":"
			Types
			FeatureOption*
			EMPTYLINE+

This is the type of the embedded content of an entity. The type of a content could be a type defined by an entity, an external type definition (uses a shortcut as a prefix followed by a colon), a predefined type, e.g. by the W3C XML specifications, and any kind of content (wildcard).

	EntityType "entity type"
		= _ ":" Type

	Type "type"
		= _ IsInstance ID ":" ID
		/ _ ID ":" ID
		/ _ "*"
		/ _ ID

These are the types a feature can have. If a feature have more than one type it have
variant types, on which an instance can choice at runtime.

	Types
		= Type VariantTypes*

	VariantTypes
		= _ "|" Type

The embedded content can be restricted by a number range, text pattern, predefined values, and length.

	EntityOption
		= Range
		/ Regex
		/ Enumeration
		/ Length

The content of a property can be restricted by a number range, text pattern, predefined values, and the length of the content. A property can occur exactly one time, can be optionally and occur multiple times. The value can be dimensioned with a unit. Further a default value can be assigned to a property.

	FeatureOption
		= Range
		/ Regex
		/ Enumeration
		/ Length
		/ IsMany
		/ IsOptional
		/ Unit
		/ DefaultValue

A number range is given by a start and end value.

	Range
		= __ "range"i __ NUMBER __ "to"i __ NUMBER

A regular expression defines a valid text pattern.

	Regex
		= __ "match"i __ Pattern

	Pattern "pattern string"
		= REGEX

Predefined values can be enumerated.

	Enumeration
		= __ "from"i EnumerationValue+

	EnumerationValue
		= __ STRING

The length of the content of a property can be restricted.

	Length
		= __ "length"i __ NUMBER

Usually a property will translated to a simple value. If want to ensure the property will be translated to a complex value, you must define it as an instance of a given type.

	IsInstance
		= "instance"i __ "of"i __

The property can occur multiple time in an instance of that entity. Typically this means that the property will be translated to a complex value, e.g. in XML as an element and not an attribute.

	IsMany
		= __ "many"i

The property is not mandatory and must not be present in an instance of the entity.

	IsOptional
		= __ "optional"i

The values of the property are dimensioned using the given unit.

	Unit
		= __ "unit"i __ ScaleUnit

	ScaleUnit "scale unit"
		= ID

This is the default value of a property, if the property is not given in an instance of the entity.

	DefaultValue
		= __ "default"i __ DefaultValueString

	DefaultValueString "string with the default value"
		= STRING

Annotations are meta information for an entity or property. They can control the behavior of the code generation.

	Annotations
		= _ "[" _ Annotation NextAnnotation* _ "]"
		/ _ "[" _ "]"
		/

	NextAnnotation
		= _ "," _ Annotation

	Annotation "annotation identifier or value"
		= ID "=" STRING
		/ ID
